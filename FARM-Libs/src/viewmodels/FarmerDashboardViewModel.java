package viewmodels;

import models.Farmer;

import java.io.Serializable;
import java.util.List;

public class FarmerDashboardViewModel implements Serializable{
	
	private Farmer farmer;
	private List<FarmerCustomer> farmerCustomers;
	private float totalEarnings;
	
	public FarmerDashboardViewModel(Farmer farmer, List<FarmerCustomer> farmerCustomers) {
		this.farmer = farmer;
		this.farmerCustomers = farmerCustomers;
		setTotalEarnings();
	}
	
	public Farmer getFarmer() {
		return farmer;
	}
	
	public void setFarmer(Farmer farmer) {
		this.farmer = farmer;
	}
	
	public List<FarmerCustomer> getFarmerCustomers() {
		return farmerCustomers;
	}
	
	public void setFarmerCustomers(List<FarmerCustomer> farmerCustomers) {
		this.farmerCustomers = farmerCustomers;
	}
	
	public float getTotalEarnings() {
		return totalEarnings;
	}
	
	private void setTotalEarnings() {
		float totalEarnings = 0.0f;
		
		for (FarmerCustomer farmerCustomer: farmerCustomers) {
			totalEarnings += farmerCustomer.getAmountSpent();
		}
		
		this.totalEarnings = totalEarnings;
	}
}

