package viewmodels;

import models.Customer;

import java.io.Serializable;

public class CustomerDashboardViewModel implements Serializable {
	
	private Customer customer;
	
	public CustomerDashboardViewModel(Customer customer) {
		this.customer = customer;
	}
	
	public Customer getCustomer() {
		return customer;
	}
	
	public void setCustomer(Customer customer) {
		this.customer = customer;
	}
}
