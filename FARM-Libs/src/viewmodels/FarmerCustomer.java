package viewmodels;

public class FarmerCustomer {
	
	private String firstName;
	private String lastName;
	private float amountSpent;
	
	public FarmerCustomer(String firstName, String lastName, float amountSpent) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.amountSpent = amountSpent;
	}
	
	public String getFirstName() {
		return firstName;
	}
	
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	
	public String getLastName() {
		return lastName;
	}
	
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	public float getAmountSpent() {
		return amountSpent;
	}
	
	public void setAmountSpent(float amountSpent) {
		this.amountSpent = amountSpent;
	}
}
