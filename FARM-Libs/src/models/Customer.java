package models;

import java.io.Serializable;
import java.util.List;

public class Customer extends User implements Serializable{

	private float accountBalance;

	private ShoppingCart shoppingCart;
	
	public Customer(String email, String password, String firstName, String lastName, byte[] photo) {
		super(email, password, firstName, lastName, photo);
		//List<E> <ShoppingItem> item;
		//item.add(0, new Crop());
		//shoppingCart.setItems( );
		setAccountBalance(0.0f);
	}

	public Customer(int id, String email, String firstName, String lastName, byte[] photo, Float accountBalance) {
		super(id, email, firstName, lastName, photo);
		setAccountBalance(accountBalance);
	}

	public float getAccountBalance() {
		return accountBalance;
	}

	public void setAccountBalance(float accountBalance) {
		this.accountBalance = accountBalance;
	}

	public ShoppingCart getShoppingCart() {
		
		return shoppingCart;
	}

	public void setShoppingCart(ShoppingCart shoppingCart) {
		this.shoppingCart = shoppingCart;
	}

}
