package models;

import java.io.Serializable;

public class Address implements Serializable{
	private String street;
	private String town;
	private String parish;

	public Address(String street, String streetName, String parish) {
		this.street = street;
		this.town = streetName;
		this.parish = parish;
	}

	public String getStreet() {
		return street;
	}
	public void setStreet(String street) {
		this.street = street;
	}
	public String getTown() {
		return town;
	}
	public void setTown(String streetName) {
		this.town = streetName;
	}
	public String getParish() {
		return parish;
	}
	public void setParish(String parish) {
		this.parish = parish;
	}

	@Override
	public String toString() {
		return "Address [Street=" + street + ", Town=" + town + ", parish=" + parish + "]";
	}
}