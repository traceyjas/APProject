package models;

import java.io.Serializable;

public class Purchase implements Serializable {

	private int FarmerId;
	private int CustomerId;
	private float amountSpent;

	public Purchase(int farmerId, int customerId, float amountSpent) {
		FarmerId = farmerId;
		CustomerId = customerId;
		this.amountSpent = amountSpent;
	}

	public int getFarmerId() {
		return FarmerId;
	}

	public void setFarmerId(int farmerId) {
		FarmerId = farmerId;
	}

	public int getCustomerId() {
		return CustomerId;
	}

	public void setCustomerId(int customerId) {
		CustomerId = customerId;
	}

	public float getAmountSpent() {
		return amountSpent;
	}

	public void setAmountSpent(float amountSpent) {
		this.amountSpent = amountSpent;
	}
}
