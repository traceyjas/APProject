package models;

public enum UserType {
    FARMER,
    CUSTOMER
}
