package models;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Farmer extends User implements Serializable{

	private Address farmAddress;

	private List<Crop> crops;
	
	public Farmer(String email, String password, String firstName, String lastName, byte[] photo, Address farmAddress) {
		super(email, password, firstName, lastName, photo);
		crops = new ArrayList<>();
		setFarmAddress(farmAddress);
	}

	public Farmer(int id, String email, String firstName, String lastName, byte[] photo, Address farmAddress) {
		super(id, email, firstName, lastName, photo);
		setFarmAddress(farmAddress);
	}

	public Address getFarmAddress() {
		return farmAddress;
	}

	public void setFarmAddress(Address farmAddress) {
		this.farmAddress = farmAddress;
	}

	public List<Crop> getCrops() {
		return crops;
	}

	public void setCrops(List<Crop> crops) {
		this.crops = crops;
	}

	@Override
	public String toString() {
		return getId()+" "+getFirstName()+" "+getLastName();
	/*	return "Farmer{" +
				"farmAddress=" + farmAddress +
				"} " + super.toString();
				
		return "" +getFullName() +"| Address= " + farmAddress.getStreet()+","+farmAddress.getTown()+","+farmAddress.getParish()+
		" " + super.toString();*/
	}
}
