package models;

import java.io.Serializable;

public class ShoppingItem implements Serializable {

	public int quantity;
	public Crop crop;

	public ShoppingItem(int quantity, Crop crop) {
		this.quantity = quantity;
		this.crop = crop;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public Crop getCrop() {
		return crop;
	}

	public void setCrop(Crop crop) {
		this.crop = crop;
	}

	@Override
	public String toString() {
		return "ShoppingItem{" +
				"quantity=" + quantity +
				", crop=" + crop +
				'}';
	}
}
