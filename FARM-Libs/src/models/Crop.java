package models;

import java.io.Serializable;

public class Crop implements Serializable{

	private int id;
	private String name;
	private byte[] image;

	private float weight;
	private int quantity;

	private float cost;

	private boolean inStock;

	private int farmerId;

	public Crop(String name, byte[] image, float weight, int quantity, float cost, int farmerId) {
		setName(name);
		setImage(image);
		setWeight(weight);
		setQuantity(quantity);
		setCost(cost);
		setInStock();
		setFarmerId(farmerId);
	}

	public Crop(int id, String name, byte[] image, float weight, int quantity, float cost, int farmerId) {
		setId(id);
		setName(name);
		setImage(image);
		setWeight(weight);
		setQuantity(quantity);
		setCost(cost);
		setInStock();
		setFarmerId(farmerId);
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public byte[] getImage() {
		return image;
	}

	public void setImage(byte[] image) {
		this.image = image;
	}

	public float getWeight() {
		return weight;
	}

	public void setWeight(float weight) {
		this.weight = weight;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		if (quantity < 0){
			// Catch Exception
			this.quantity = 0;
		} else {
			this.quantity = quantity;
		}
	}

	public float getCost() {
		return cost;
	}

	public void setCost(float cost) {
		if (cost < 0){
			// Catch Exception
			this.cost = 0.0f;
		} else {
			this.cost = cost;
		}
	}

	public boolean getInStock() {
		return inStock;
	}

	private void setInStock() {

		if (quantity > 0){
			this.inStock = true;
		} else {
			this.inStock = false;
		}
	}

	public int getFarmerId() {
		return farmerId;
	}

	public void setFarmerId(int farmerId) {
		this.farmerId = farmerId;
	}

	@Override
	public String toString() {
		return name;
				/*"Crop " +
				"name='" + name + '\'' +
				", image='" + image + '\'' +
				", weight=" + weight +
				", quantity=" + quantity +
				", cost=" + cost +
				", inStock=" + inStock;*/
	}
}
