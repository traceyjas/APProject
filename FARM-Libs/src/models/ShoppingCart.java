package models;

import java.io.Serializable;
import java.util.List;

public class ShoppingCart implements Serializable {

	private List<ShoppingItem> items;
	private float total;

	public void addCrop(ShoppingItem item){
		items.add(item);
	}

	public List<ShoppingItem> getItems() {
		return items;
	}

	public void setItems(List<ShoppingItem> items) {
		this.items = items;
	}

	public float getTotal() {
		return total;
	}

	public void setTotal(float total) {
		this.total = total;
	}


}
