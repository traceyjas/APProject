package resources;

import java.io.Serializable;

public class Response implements Serializable {

    private static final long serialVersionUID = 1L;
    private boolean success; //whether the action was successful or not
    private String errorMessage; //any specific error details that would assist the client in debugging the errors
    private Object result; //data returned

    public Response(Object result) {
        this(result,true,"");
    }

    public Response(Object result, boolean success, String errorMessage ) {
        super();
        this.success = success;
        this.errorMessage = errorMessage;
        this.result = result;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public Object getResult() {
        return result;
    }

    public void setResult(Object result) {
        this.result = result;
    }

    @Override
    public String toString() {
        return "Response{" +
                "success=" + success +
                ", errorMessage='" + errorMessage + '\'' +
                ", result=" + result +
                '}';
    }
}
