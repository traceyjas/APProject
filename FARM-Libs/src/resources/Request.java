package resources;

import java.io.Serializable;

public class Request implements Serializable {

    private static final long serialVersionUID = 1L;
    private String action; //action to be invoked
    private Object data; //data / parameter
    private Object data2; 


    public Request(String action, Object data) {
        super();
        this.action = action;
        this.data = data;
        this.data2 = null;
    }
    
    public Request(String action, Object data, Object data2) {
        super();
        this.action = action;
        this.data = data;
        this.data2 = data2;
    }

    public Request(String action) {
        this(action,null,null);
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public Object getData() {
        return data;
    }
    
    public Object getData2() {
        return data2;
    }

    public void setData(Object data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "Request{" +
                "action='" + action + '\'' +
                ", data=" + data +
                '}';
    }
}
