package views;

import java.awt.*;
import java.awt.image.BufferedImage;

public class ResizeImage {
	public BufferedImage scaleImage(int w, int h, BufferedImage img) throws Exception {
		BufferedImage bi;
		bi = new BufferedImage(w, h, BufferedImage.TRANSLUCENT);
		Graphics2D g2d = (Graphics2D) bi.createGraphics();
		g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		g2d.addRenderingHints(new RenderingHints(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY));
		g2d.drawImage(img, 0, 0, w, h, null);
		g2d.dispose();
		return bi;
	}

	public Image resImage(Image img){

		BufferedImage ri = new BufferedImage(118,95, BufferedImage.TYPE_INT_RGB);
		Graphics2D g = ri.createGraphics();
		g.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
		g.drawImage(img, 0, 0, 118, 95, null);
		g.dispose();
		return ri;

	}
	
	public Image resImage2(Image img){
		BufferedImage ri = new BufferedImage(180,145, BufferedImage.TYPE_INT_RGB);
		Graphics2D g = ri.createGraphics();
		g.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
		g.drawImage(img, 0, 0, 180, 145, null);
		g.dispose();
		return ri;
	}

}
