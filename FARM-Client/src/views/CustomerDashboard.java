package views;

import controllers.CropController;
import controllers.CustomerController;
import controllers.FarmerController;
import models.Crop;
import resources.Response;
import viewmodels.CustomerDashboardViewModel;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;

import models.Customer;
import models.CustomerClient;
import models.Farmer;
import models.FarmerClient;
import models.ShoppingItem;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.ByteArrayInputStream;

public class CustomerDashboard extends JPanel {

	private static final long serialVersionUID = 1L;
	private JTextField txtAccountBalance;
	private JTextField txtQuantity;
	private JLabel amountLabel;
	//private JTable tableAvailableCrops;
	private CustomerDashboardViewModel customerDashboardViewModel;
	private CustomerController customerController = new CustomerController(new CustomerClient());

	private FarmerController farmerController =  new FarmerController(new FarmerClient());
	private CropController   cropController = new CropController(new FarmerClient());

//	CustomerController cc;	


	JComboBox<Crop> cbxSelectCrop = new JComboBox<Crop>();

//	JComboBox<String> cbxSelectCrop = new JComboBox<String>();

	JComboBox<Farmer> cbxSelectFarmer = new JComboBox<Farmer>();
	
	private JTable tableAvailableCrops;
	//private CustomerDashboardViewModel customerDashboardViewModel; already in constructor

//	private CustomerController customerController = new CustomerController(new CustomerClient());
	private JTable tableShoopingCart;

	public CustomerDashboard(CustomerDashboardViewModel customerDashboardViewModel) {
		this.customerDashboardViewModel = customerDashboardViewModel;
		
		setBounds(0, 0, 1357, 650);
		setBackground(new Color(204, 255, 255));
		setBorder(new EmptyBorder(5, 5, 5, 5));
		setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setVisible(false);
		panel.setBackground(new Color(204, 255, 153, 190));
		panel.setBounds(436, 120, 260, 83);
		
		panel.setLayout(null);
		
		add(panel,BorderLayout.NORTH);

		JLabel lblUpdateAccountBalance = new JLabel("UPDATE ACCOUNT BALANCE");
		lblUpdateAccountBalance.setBounds(10, 5, 168, 22);
		lblUpdateAccountBalance.setForeground(new Color(204, 0, 0));
		lblUpdateAccountBalance.setFont(new Font("Rockwell Condensed", Font.PLAIN, 18));
		panel.add(lblUpdateAccountBalance);

		amountLabel = new JLabel();
		amountLabel.setText((String.format("%.2f", customerDashboardViewModel.getCustomer().getAccountBalance())));
		amountLabel.setBounds(178, 45, 107, 20);

		JButton btnSubmit = new JButton("Submit");

		btnSubmit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				float balanceUpdate = Float.parseFloat(txtAccountBalance.getText()) + Float.parseFloat(amountLabel.getText());

				customerDashboardViewModel.getCustomer().setAccountBalance(balanceUpdate);
				Response response = customerController.updateBalance(customerDashboardViewModel.getCustomer());
				if (response.isSuccess())
				{
					JOptionPane.showMessageDialog(null, "Updated Balance", "Balance Changed", JOptionPane.INFORMATION_MESSAGE);
					amountLabel.setText((String.format("%.2f", customerDashboardViewModel.getCustomer().getAccountBalance())));
					customerDashboardViewModel.setCustomer( (Customer) response.getResult());
					txtAccountBalance.setText(null);
					panel.setVisible(false);
				}
				else{
					JOptionPane.showMessageDialog(null, "Balance unsccessfully updated", "Error", JOptionPane.INFORMATION_MESSAGE);
				}
				panel.setVisible(false);

			}
		});
		
		
		
		btnSubmit.setForeground(Color.WHITE);
		btnSubmit.setFont(new Font("SansSerif", Font.PLAIN, 12));
		btnSubmit.setBackground(new Color(204, 0, 0));
		btnSubmit.setBounds(173, 38, 75, 23);
		panel.add(btnSubmit);
		
		JLabel lblAmount = new JLabel("Amount:");
		lblAmount.setFont(new Font("SansSerif", Font.BOLD, 12));
		lblAmount.setBounds(10, 42, 57, 14);
		panel.add(lblAmount);

		txtAccountBalance = new JTextField();
		txtAccountBalance.setBounds(77, 40, 86, 20);
		panel.add(txtAccountBalance);
		txtAccountBalance.setColumns(10);

		JLabel label_1 = new JLabel("$");
		label_1.setFont(new Font("SansSerif", Font.BOLD, 12));
		label_1.setBounds(66, 42, 14, 14);
		panel.add(label_1);

		JButton button_3 = new JButton("-");
		button_3.setBackground(new Color(204, 255, 255));
		button_3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				panel.setVisible(false);
			}
		});
		button_3.setBounds(214, 0, 46, 23);
		panel.add(button_3);

		JLabel label = new JLabel("DASHBOARD");
		label.setForeground(new Color(204, 0, 0));
		label.setFont(new Font("Rockwell Extra Bold", Font.PLAIN, 25));
		label.setBounds(618, 0, 239, 41);
		add(label);

		JPanel panel_5 = new JPanel();
		panel_5.setLayout(null);
		panel_5.setBackground(new Color(204, 134, 145, 123));
		panel_5.setBounds(29, 35, 762, 26);
		add(panel_5);

		JPanel panel_4 = new JPanel();
		panel_4.setVisible(false);
		panel_4.setBackground(new Color(204, 255, 153, 200));
		panel_4.setBounds(809, 60, 497, 418);
		add(panel_4);
		panel_4.setLayout(null);

		JButton btnViewShoppingKart = new JButton("View Shopping Cart");
		btnViewShoppingKart.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				panel_4.setVisible(true);
			}
		});
		btnViewShoppingKart.setBounds(346, 0, 147, 25);
		panel_5.add(btnViewShoppingKart);
		btnViewShoppingKart.setForeground(Color.WHITE);
		btnViewShoppingKart.setFont(new Font("SansSerif", Font.PLAIN, 12));
		btnViewShoppingKart.setBackground(new Color(204, 0, 0));

		JPanel panel_2 = new JPanel();
		panel_2.setVisible(false);
		panel_2.setLayout(null);
		panel_2.setBackground(new Color(204, 134, 145, 200));
		panel_2.setBounds(809, 478, 497, 171);
		add(panel_2);

		JButton btnMakePurchase = new JButton("Make Purchase");
		btnMakePurchase.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				panel_2.setVisible(true);
			}
		});
		btnMakePurchase.setBounds(498, 1, 127, 24);
		panel_5.add(btnMakePurchase);
		btnMakePurchase.setForeground(Color.WHITE);
		btnMakePurchase.setFont(new Font("SansSerif", Font.PLAIN, 12));
		btnMakePurchase.setBackground(new Color(204, 0, 0));

		JPanel panel_1 = new JPanel();
		panel_1.setVisible(false);
		panel_1.setBackground(new Color(204, 255, 153, 200));
		panel_1.setBounds(29, 225, 395, 420);
		add(panel_1);
		panel_1.setLayout(null);

		JPanel panel_3 = new JPanel();
		panel_3.setVisible(false);
		panel_3.setLayout(null);
		panel_3.setBackground(new Color(204, 255, 153));//200
		panel_3.setBounds(424, 307, 260, 338);
		add(panel_3);

		
		JButton btnViewCropInformation = new JButton("View Crop Information");
		btnViewCropInformation.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				cbxSelectFarmer.addActionListener(new ActionListener() {			
					@Override
					public void actionPerformed(ActionEvent e) {
						//System.out.println("item searched");
						//System.out.println(cbxSelectFarmer.getSelectedItem());
						Response response = cropController.get(( (Farmer) cbxSelectFarmer.getSelectedItem() ).getId());
						//System.out.println(( (Farmer) cbxSelectFarmer.getSelectedItem() ).getId());
						
						if (response.isSuccess())
						{
							//JOptionPane.showMessageDialog(null, "Working");
							java.util.List<Crop> c  = (java.util.List<Crop>) response.getResult();
							cbxSelectCrop.removeAllItems();
							for (Crop cropitem : c){
								 cbxSelectCrop.addItem(cropitem);
							}
						}
					}
				});
				 
					panel_1.setVisible(true);
					panel_3.setVisible(true);
			}
		});
		
		btnViewCropInformation.setForeground(new Color(255, 255, 255));
		btnViewCropInformation.setFont(new Font("SansSerif", Font.PLAIN, 12));
		btnViewCropInformation.setBackground(new Color(204, 0, 0));
		btnViewCropInformation.setBounds(180, 0, 160, 25);
		panel_5.add(btnViewCropInformation);

		JButton btnUpdateAccountBalance = new JButton("Update Account Balance");
		btnUpdateAccountBalance.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				panel.setVisible(true);
			}
		});
		btnUpdateAccountBalance.setForeground(Color.WHITE);
		btnUpdateAccountBalance.setFont(new Font("SansSerif", Font.PLAIN, 12));
		btnUpdateAccountBalance.setBackground(new Color(204, 0, 0));
		btnUpdateAccountBalance.setBounds(0, 0, 175, 25);
		panel_5.add(btnUpdateAccountBalance);

		JButton btnCollapseAll = new JButton("Collapse All");
		btnCollapseAll.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				panel.setVisible(false);
				panel_1.setVisible(false);
				panel_2.setVisible(false);
				panel_3.setVisible(false);
				panel_4.setVisible(false);
			}
		});
		btnCollapseAll.setBounds(633, 1, 127, 24);
		panel_5.add(btnCollapseAll);
		btnCollapseAll.setForeground(Color.WHITE);
		btnCollapseAll.setFont(new Font("SansSerif", Font.PLAIN, 12));
		btnCollapseAll.setBackground(new Color(204, 0, 0));



		JLabel lblCropInformation = new JLabel("CROP INFORMATION");
		lblCropInformation.setBounds(87, 0, 168, 22);
		lblCropInformation.setForeground(new Color(204, 0, 0));
		lblCropInformation.setFont(new Font("Rockwell Condensed", Font.PLAIN, 18));
		panel_1.add(lblCropInformation);

		JLabel lblAvailableCrops = new JLabel("Available crops:");
		lblAvailableCrops.setFont(new Font("SansSerif", Font.BOLD, 12));
		lblAvailableCrops.setBounds(10, 33, 93, 14);
		panel_1.add(lblAvailableCrops);
		
			//Search farmer function
		JTextField txtfieldFarmerSearch1 = new JTextField("Enter firstname of Farmer to search for");
		JTextField txtfieldFarmerSearch2 = new JTextField("Enter lastname of Farmer to search for");
		
		tableAvailableCrops = new JTable();
		tableAvailableCrops.setLocation(0, 58);
		//tableAvailableCrops.setBounds(258, 428, -237, -248);
		tableAvailableCrops.setSize(395,362);
		panel_1.add(tableAvailableCrops);

		JButton button_1 = new JButton("-");
		button_1.setBackground(new Color(204, 255, 255));
		button_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				panel_1.setVisible(false);
				panel_3.setVisible(false);
			}
		});
		button_1.setBounds(349, 2, 46, 23);
		panel_1.add(button_1);

		JLabel lblPickupLocation = new JLabel("Pickup Location:");
		lblPickupLocation.setFont(new Font("SansSerif", Font.BOLD, 12));
		lblPickupLocation.setBounds(20, 21, 130, 14);
		panel_2.add(lblPickupLocation);

		ButtonGroup rdbtnklp = new ButtonGroup();
		JRadioButton rdbtnKingston = new JRadioButton("Kingston Market");
		rdbtnKingston.setBackground(new Color(255, 153, 255));
		rdbtnKingston.setBounds(20, 57, 119, 23);
		panel_2.add(rdbtnKingston);

		JRadioButton rdbtnLinsteadMarket = new JRadioButton("Linstead Market");
		rdbtnLinsteadMarket.setBackground(new Color(255, 153, 255));
		rdbtnLinsteadMarket.setBounds(150, 57, 121, 23);
		panel_2.add(rdbtnLinsteadMarket);

		JRadioButton rdbtnPapineMarket = new JRadioButton("Papine Market");
		rdbtnPapineMarket.setBackground(new Color(255, 153, 255));
		rdbtnPapineMarket.setBounds(285, 57, 109, 23);
		panel_2.add(rdbtnPapineMarket);
		
		rdbtnklp.add(rdbtnPapineMarket);
		rdbtnklp.add(rdbtnLinsteadMarket);
		rdbtnklp.add(rdbtnKingston);

		JButton btnMakePurchase1 = new JButton("Make Purchase");
		btnMakePurchase1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		btnMakePurchase1.setForeground(new Color(0, 0, 0));
		btnMakePurchase1.setFont(new Font("SansSerif", Font.PLAIN, 12));
		btnMakePurchase1.setBackground(new Color(255, 255, 255));
		btnMakePurchase1.setBounds(169, 120, 154, 40);
		panel_2.add(btnMakePurchase1);

		JButton button_5 = new JButton("-");
		button_5.setBackground(new Color(204, 255, 255));
		button_5.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				panel_2.setVisible(false);
			}
		});
		button_5.setBounds(451, 0, 46, 23);
		panel_2.add(button_5);

		JLabel lblAddToShopping = new JLabel("ADD TO CART");
		lblAddToShopping.setForeground(new Color(204, 0, 0));
		lblAddToShopping.setFont(new Font("Rockwell Condensed", Font.PLAIN, 18));
		lblAddToShopping.setBounds(96, 0, 154, 22);
		panel_3.add(lblAddToShopping);

		JLabel label_3 = new JLabel("Select crop:");
		label_3.setFont(new Font("SansSerif", Font.BOLD, 12));
		label_3.setBounds(20, 151, 92, 14);
		panel_3.add(label_3);

		JLabel label_5 = new JLabel("Quantity:");
		label_5.setFont(new Font("SansSerif", Font.BOLD, 12));
		label_5.setBounds(10, 221, 67, 14);
		panel_3.add(label_5);

		txtQuantity = new JTextField();
		txtQuantity.setColumns(10);
		txtQuantity.setBounds(74, 219, 86, 20);
		panel_3.add(txtQuantity);

		JButton btnAddItem = new JButton("Add Item");
		btnAddItem.setForeground(Color.WHITE);
		btnAddItem.setFont(new Font("SansSerif", Font.PLAIN, 12));
		btnAddItem.setBackground(new Color(204, 0, 0));
		btnAddItem.setBounds(54, 279, 154, 34);
		btnAddItem.setBounds(25, 280, 154, 34);
		btnAddItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				
					Crop crop = ((Crop) cbxSelectCrop.getSelectedItem());
					if (crop.getQuantity() == 0)
					{
						System.out.println("Cannot Purchase Crop, no quantity remaining");
						return;
					}
					if (crop.getQuantity()- Integer.parseInt(txtQuantity.getText()) < 0)
					{
						System.out.println("Not enough crops remaining, only "+ crop.getQuantity()+" remaining" );
						return;
					}
							
				//((Crop) cbxSelectCrop.getSelectedItem()).setQuantity(((Crop) cbxSelectCrop.getSelectedItem()).getQuantity()-1);
				Customer customer = customerDashboardViewModel.getCustomer();
<<<<<<< HEAD
				//customer.getShoppingCart().addCrop(new ShoppingItem(Integer.parseInt(txtQuantity.getText() ), crop));
=======
			//	customer.getShoppingCart().addCrop(new ShoppingItem(Integer.parseInt(txtQuantity.getText() ), crop));
>>>>>>> 667d85b708a710d64229ddf4938ece7c8af91281
				customerDashboardViewModel.setCustomer(customer);
				crop.setQuantity(crop.getQuantity()- Integer.parseInt(txtQuantity.getText()) );
				Response response = cropController.update(crop);
				if (response.isSuccess())
				{
					 response = cropController.get(( (Farmer) cbxSelectFarmer.getSelectedItem() ).getId());
					//	System.out.println(( (Farmer) cbxSelectFarmer.getSelectedItem() ).getId());
					 	if (response.isSuccess())
					 	{
					 		JOptionPane.showMessageDialog(null, "Crop added to cart","Successfully added",JOptionPane.INFORMATION_MESSAGE);
					 		 java.util.List<Crop> c  = (java.util.List<Crop>)	response.getResult();
					 		 cbxSelectCrop.removeAllItems();
					 		for (Crop cropitem : c )
					 			cbxSelectCrop.addItem(cropitem);
					 	}
				}
				txtQuantity.setText(null);
			}
		});
		panel_3.add(btnAddItem);
		
		JLabel lblSelectFarmer = new JLabel("Select Farmer:");
		lblSelectFarmer.setFont(new Font("SansSerif", Font.BOLD, 12));
		lblSelectFarmer.setBounds(10, 78, 102, 17);
		panel_3.add(lblSelectFarmer);

		cbxSelectFarmer.setBounds(132, 77, 28, 20);
		
		cbxSelectFarmer = new JComboBox();
		//cbxSelectFarmer.addItem(item);
		cbxSelectFarmer.setBounds(113, 77, 139, 20);
		panel_3.add(cbxSelectFarmer);

		    panel_3.add(cbxSelectCrop);
		   
	//	cbxSelectCrop.setBounds(123, 69, 120, 20);
		
		cbxSelectCrop.setBounds(113, 149, 137, 20);
		
		JButton btnNewButton = new JButton("Refresh");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				Response response = farmerController.getAllFarmers();
				 if (response.isSuccess()) {
					 JComboBox<Farmer> list = new JComboBox<Farmer>();
				 	 java.util.List<Farmer> t  = (java.util.List<Farmer>)	response.getResult();
				 	 cbxSelectFarmer.removeAllItems();
				 	 for (Farmer s: t )
				 		cbxSelectFarmer.addItem(s);
				 }
				 
				 btnNewButton.setVisible(false);
			}
		});
		btnNewButton.setBackground(Color.WHITE);
		btnNewButton.setBounds(161, 33, 89, 23);
		panel_3.add(btnNewButton);
		//panel_3.add(cbxSelectCrop);

		JLabel lblShoppingKart = new JLabel("SHOPPING CART");
		lblShoppingKart.setForeground(new Color(204, 0, 0));
		lblShoppingKart.setFont(new Font("Rockwell Condensed", Font.PLAIN, 18));
		lblShoppingKart.setBounds(214, 0, 168, 22);
		panel_4.add(lblShoppingKart);

		JButton button_4 = new JButton("-");
		button_4.setBackground(new Color(204, 255, 255));
		button_4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				panel_4.setVisible(false);
			}
		});
		button_4.setBounds(451, 0, 46, 23);
		panel_4.add(button_4);
		
		tableShoopingCart = new JTable();
		tableShoopingCart.setBounds(0, 44, 497, 374);
		panel_4.add(tableShoopingCart);

		JButton btnLogOut = new JButton("LOG OUT");
		btnLogOut.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setVisible(false);
			}
		});
		btnLogOut.setFont(new Font("SansSerif", Font.BOLD, 12));
		btnLogOut.setBackground(Color.RED);
		btnLogOut.setBounds(1203, 14, 122, 35);
		add(btnLogOut);

		JPanel panel_6 = new JPanel();
		panel_6.setLayout(null);
		panel_6.setBackground(new Color(204, 134, 145, 123));
		panel_6.setBounds(29, 70, 345, 137);
		add(panel_6);

		JLabel lblPaulWalker = new JLabel(customerDashboardViewModel.getCustomer().getFullName());
		lblPaulWalker.setForeground(new Color(0, 0, 0));
		lblPaulWalker.setFont(new Font("Rockwell Condensed", Font.PLAIN, 18));
		lblPaulWalker.setBounds(35, 0, 168, 22);
		panel_6.add(lblPaulWalker);

		JLabel lblCurrentBalance_1 = new JLabel("CURRENT BALANCE");
		lblCurrentBalance_1.setBounds(168, 22, 149, 14);
		panel_6.add(lblCurrentBalance_1);

		panel_6.add(amountLabel);

		JLabel label_7 = new JLabel("$");
		label_7.setBounds(168, 47, 14, 14);
		panel_6.add(label_7);
		label_7.setFont(new Font("SansSerif", Font.BOLD, 12));

		Border border = BorderFactory.createLineBorder(Color.BLACK, 1);
		JLabel lblCustomerPhoto = new JLabel("");
		lblCustomerPhoto.setBorder(border);
		lblCustomerPhoto.setBounds(10, 22, 122, 104);
		ImageIcon logo;
		byte[] userpic;
		userpic = customerDashboardViewModel.getCustomer().getPhoto();
		ResizeImage r = new ResizeImage();
		try{
			logo = new ImageIcon(r.scaleImage(122,104, ImageIO.read(new ByteArrayInputStream(userpic))));
			lblCustomerPhoto.setIcon(logo);
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		panel_6.add(lblCustomerPhoto);



		JLabel label_2 = new JLabel("");
		label_2.setIcon(new ImageIcon(CustomerDashboard.class.getResource("/images/home-livestock-wildfire.jpg")));
		label_2.setBounds(-13, 0, 1400, 650);
		add(label_2);
	}
}