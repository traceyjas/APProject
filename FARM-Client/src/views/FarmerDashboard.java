package views;

import componentsForViews.CropTable;
import controllers.CropController;
import models.Crop;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import resources.Response;
import viewmodels.FarmerDashboardViewModel;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.filechooser.FileSystemView;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.List;

public class FarmerDashboard extends JPanel {
	
	private Logger logger = LogManager.getLogger(FarmerDashboard.class);
	private FarmerDashboardViewModel farmerDashboardViewModel;
	private CropController cropController;
	
	private JLabel weightLabel;
	private JLabel lbsLabel;
	private JLabel costLabel;
	private JLabel perUnitLabel;
	private JLabel quantityAvailableLabel;
	private JLabel uploadImageLabel;
	private JLabel uploadedImage;
	private JLabel uploadedImage1;
	private JButton uploadImageBtn;
	private JButton clearBtn;
	private JButton minimizeBtn;
	
	private JTable tabViewCrops;
	private JScrollPane scrollPane;
	private List<Crop> crops;
	private JTable cropTable;
	private JTable customerForFarmerTable;
	
	private CropTable ct = new CropTable();
	
	
	String data[][] = {{"Banana", "image", "50", "20", "100", "yes", "John Brown"},
	};
	String column[] = {"Name", "Photo", "Weight", "Cost", "Quantity", "Available", "Farmer Name"};
	
	
	JComboBox<Crop> list = new JComboBox<Crop>();
	private int cropIndex = 0;
	
	public FarmerDashboard(CropController cropController, FarmerDashboardViewModel farmerDashboardViewModel) {
		this.cropController = cropController;
		this.farmerDashboardViewModel = farmerDashboardViewModel;
		configureView();
	}
	
	private void configureView() {
		setBounds(100, 100, 1357, 650);
		setBackground(new Color(204, 255, 153));
		setBorder(new EmptyBorder(5, 5, 5, 5));
		setLayout(null);
		
		JLabel dashboardLabel = new JLabel("DASHBOARD");
		dashboardLabel.setForeground(new Color(255, 255, 255));
		dashboardLabel.setFont(new Font("Rockwell Extra Bold", Font.PLAIN, 25));
		dashboardLabel.setBounds(577, 0, 200, 25);
		add(dashboardLabel);
		
		configureInfoPanel();
		
		JPanel addCropPanel = configureAddCropPanel();
		add(addCropPanel);
		
		JPanel updateCropPanel = configureUpdateCropPanel();
		add(updateCropPanel);
		
		JPanel viewCropsPanel = configureViewCropsPanel();
		add(viewCropsPanel);
		
		JPanel viewCustomersPanel = configureViewCustomersPanel();
		add(viewCustomersPanel);
		
		JButton SignOutBtn = new JButton("LOG OUT");
		SignOutBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setVisible(false);
			}
		});
		SignOutBtn.setBackground(new Color(255, 0, 0));
		SignOutBtn.setFont(new Font("SansSerif", Font.BOLD, 12));
		SignOutBtn.setBounds(1210, 7, 122, 35);
		add(SignOutBtn);
		
		JButton addNewCropBtn = new JButton("ADD A NEW CROP");
		addNewCropBtn.setForeground(new Color(0, 102, 0));
		addNewCropBtn.setBounds(52, 30, 135, 23);
		add(addNewCropBtn);
		addNewCropBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				addCropPanel.setVisible(true);
			}
		});
		
		JButton updateCropDetailsBtn = new JButton("UPDATE CROP DETAILS");
		updateCropDetailsBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				updateCropPanel.setVisible(true);
			}
		});
		updateCropDetailsBtn.setForeground(new Color(0, 102, 0));
		updateCropDetailsBtn.setBounds(196, 30, 170, 23);
		add(updateCropDetailsBtn);
		
		JButton viewCropsBtn = new JButton("VIEW MY CROPS");
		viewCropsBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				viewCropsPanel.setVisible(true);
			}
		});
		viewCropsBtn.setForeground(new Color(0, 102, 0));
		viewCropsBtn.setBounds(378, 30, 135, 23);
		add(viewCropsBtn);
		
		JButton viewCustomersBtn = new JButton("VIEW MY CUSTOMERS");
		viewCustomersBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				viewCustomersPanel.setVisible(true);
			}
		});
		viewCustomersBtn.setForeground(new Color(0, 102, 0));
		viewCustomersBtn.setBounds(523, 30, 167, 23);
		add(viewCustomersBtn);
		
		JButton collapseAllBtn = new JButton("COLLAPSE ALL");
		collapseAllBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				addCropPanel.setVisible(false);
				updateCropPanel.setVisible(false);
				viewCropsPanel.setVisible(false);
				viewCustomersPanel.setVisible(false);
			}
		});
		collapseAllBtn.setForeground(new Color(0, 102, 0));
		collapseAllBtn.setBounds(700, 30, 122, 23);
		add(collapseAllBtn);
		
		JLabel backgroundLabel = new JLabel("");
		backgroundLabel.setIcon(new ImageIcon(FarmerDashboard.class.getResource("/images/f3.jpg")));
		backgroundLabel.setBounds(0, 0, 1373, 715);
		add(backgroundLabel);
	}
	
	private void configureInfoPanel() {
		JPanel infoPanel = new JPanel();
		infoPanel.setBackground(new Color(204, 193, 255, 180));
		infoPanel.setBounds(53, 60, 637, 190);
		add(infoPanel);
		infoPanel.setLayout(null);
		
		JLabel nameLabel = new JLabel(farmerDashboardViewModel.getFarmer().getFullName());
		nameLabel.setFont(new Font("Rockwell Extra Bold", Font.PLAIN, 20));
		nameLabel.setForeground(new Color(204, 0, 0));
		nameLabel.setBounds(200, 0, 250, 24);
		infoPanel.add(nameLabel);
		
		JLabel lblEmail = new JLabel(farmerDashboardViewModel.getFarmer().getEmail());
		lblEmail.setBounds(235, 47, 250, 14);
		infoPanel.add(lblEmail);
		
		JLabel lblAddress = new JLabel(farmerDashboardViewModel.getFarmer().getFarmAddress().getStreet());
		lblAddress.setBounds(235, 97, 250, 14);
		infoPanel.add(lblAddress);
		
		JLabel label = new JLabel(farmerDashboardViewModel.getFarmer().getFarmAddress().getTown());
		label.setVerticalAlignment(SwingConstants.TOP);
		label.setBounds(235, 72, 250, 14);
		infoPanel.add(label);
		
		JLabel label_1 = new JLabel(farmerDashboardViewModel.getFarmer().getFarmAddress().getParish());
		label_1.setBounds(235, 122, 250, 14);
		infoPanel.add(label_1);
		
		JLabel lblTotalEarnings = new JLabel("TOTAL EARNINGS: $" + farmerDashboardViewModel.getTotalEarnings());
		lblTotalEarnings.setFont(new Font("SansSerif", Font.BOLD, 12));
		lblTotalEarnings.setForeground(Color.RED);
		lblTotalEarnings.setBounds(171, 165, 250, 14);
		infoPanel.add(lblTotalEarnings);
		
		Border border = BorderFactory.createLineBorder(Color.BLACK, 1);
		JLabel labelFarmerPhoto = new JLabel("");
		labelFarmerPhoto.setBorder(border);
		labelFarmerPhoto.setBounds(20, 21, 148, 140);
		
		ImageIcon logo;
		byte[] userpic = farmerDashboardViewModel.getFarmer().getPhoto();
		ResizeImage r = new ResizeImage();
		
		logger.info("Attempting to set farmer image");
		try {
			logo = new ImageIcon(r.scaleImage(148, 140, ImageIO.read(new ByteArrayInputStream(userpic))));
			labelFarmerPhoto.setIcon(logo);
		} catch (Exception e) {
			logger.error("Unable to set farmer image.", e);
		}
		
		infoPanel.add(labelFarmerPhoto);
	}
	
	private JPanel configureAddCropPanel() {
		JPanel panel = new JPanel();
		panel.setVisible(false);
		panel.setBackground(new Color(204, 193, 255, 200));
		panel.setBounds(52, 257, 327, 392);
		panel.setLayout(null);
		
		JLabel newCropLabel = new JLabel("ADD A NEW CROP");
		newCropLabel.setForeground(new Color(204, 0, 0));
		newCropLabel.setFont(new Font("Rockwell Condensed", Font.PLAIN, 18));
		newCropLabel.setBounds(120, 11, 120, 14);
		panel.add(newCropLabel);
		
		JLabel nameLabel = new JLabel("Name:");
		nameLabel.setFont(new Font("SansSerif", Font.BOLD, 12));
		nameLabel.setBounds(44, 44, 46, 14);
		panel.add(nameLabel);
		
		JTextField nameTextField = new JTextField(10);
		nameTextField.setBounds(100, 41, 162, 20);
		panel.add(nameTextField);
		
		weightLabel = new JLabel("Weight:");
		weightLabel.setFont(new Font("SansSerif", Font.BOLD, 12));
		weightLabel.setBounds(44, 75, 46, 14);
		panel.add(weightLabel);
		
		JTextField weightTextField = new JTextField("", 10);
		weightTextField.setBounds(100, 72, 86, 20);
		weightTextField.setColumns(10);
		panel.add(weightTextField);
		
		lbsLabel = new JLabel("(lbs)");
		lbsLabel.setFont(new Font("SansSerif", Font.BOLD, 12));
		lbsLabel.setBounds(194, 75, 46, 14);
		panel.add(lbsLabel);
		
		costLabel = new JLabel("Cost:");
		costLabel.setFont(new Font("SansSerif", Font.BOLD, 12));
		costLabel.setBounds(44, 106, 46, 14);
		panel.add(costLabel);
		
		JTextField costTextField = new JTextField(10);
		costTextField.setBounds(100, 103, 86, 20);
		panel.add(costTextField);
		
		perUnitLabel = new JLabel("(per unit)");
		perUnitLabel.setFont(new Font("SansSerif", Font.BOLD, 12));
		perUnitLabel.setBounds(194, 106, 68, 14);
		panel.add(perUnitLabel);
		
		quantityAvailableLabel = new JLabel("Quantity available:");
		quantityAvailableLabel.setFont(new Font("SansSerif", Font.BOLD, 12));
		quantityAvailableLabel.setBounds(44, 146, 109, 14);
		panel.add(quantityAvailableLabel);
		
		JTextField quantityTextField = new JTextField(10);
		quantityTextField.setBounds(154, 143, 86, 20);
		panel.add(quantityTextField);
		
		uploadImageLabel = new JLabel("Upload image:");
		uploadImageLabel.setFont(new Font("SansSerif", Font.BOLD, 12));
		uploadImageLabel.setBounds(44, 232, 89, 14);
		panel.add(uploadImageLabel);
		
		Border border = BorderFactory.createLineBorder(Color.BLACK, 1);
		uploadedImage = new JLabel("");
		uploadedImage.setBorder(border);
		uploadedImage.setBounds(15, 251, 118, 96);
		panel.add(uploadedImage);
		
		uploadImageBtn = new JButton("Click to upload image");
		uploadImageBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				JFileChooser fc = new JFileChooser(FileSystemView.getFileSystemView().getHomeDirectory());
				int rv = fc.showOpenDialog(null);
				
				if (rv == JFileChooser.APPROVE_OPTION) {
					try {
						BufferedImage img = ImageIO.read(fc.getSelectedFile());
						ResizeImage ri = new ResizeImage();
						Image img2 = ri.resImage(img);
						
						uploadedImage.setIcon(new ImageIcon(img2));
						uploadedImage.setVisible(true);
						
					} catch (IOException e) {
						logger.error("Unable to upload image.", e);
					}
					
				}
			}
		});
		uploadImageBtn.setBounds(140, 229, 162, 23);
		panel.add(uploadImageBtn);
		
		minimizeBtn = new JButton("-");
		minimizeBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				panel.setVisible(false);
			}
		});
		minimizeBtn.setBounds(281, 0, 46, 23);
		panel.add(minimizeBtn);
		
		JButton addCropButton = new JButton("Submit");
		addCropButton.setFont(new Font("SansSerif", Font.PLAIN, 12));
		addCropButton.setBackground(new Color(102, 204, 51));
		addCropButton.setForeground(Color.WHITE);
		addCropButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				logger.info("Attempting to add new crop");
				
				Crop newCrop = new Crop(nameTextField.getText(), null, Float.parseFloat(weightTextField.getText()), Integer.parseInt(quantityTextField.getText()), Float.parseFloat(costTextField.getText()), farmerDashboardViewModel.getFarmer().getId());
				
				Response response = cropController.create(newCrop);
				
				if (response.isSuccess()) {
					logger.info("new crop added");
					JOptionPane.showMessageDialog(null, "Added new crop", "Crop Added", JOptionPane.INFORMATION_MESSAGE);
					
					//Update ID number of crops added without refreshing access to database
					for (Crop crop : farmerDashboardViewModel.getFarmer().getCrops()) {
						if (crop.getId() > newCrop.getId()) {
							cropIndex = cropIndex + 1;
							logger.info("New crop index " + cropIndex);
							newCrop.setId(crop.getId() + cropIndex);
						}
					}
					
					// Update crops table
					list.addItem(newCrop);
					
					// Clear form
					nameTextField.setText(null);
					weightTextField.setText(null);
					costTextField.setText(null);
					quantityTextField.setText(null);
					uploadedImage.setVisible(false);
				} else {
					logger.error("New crop not added");
					JOptionPane.showMessageDialog(null, "New crop not added", "Add Crop", JOptionPane.ERROR_MESSAGE);
				}
			}
		});
		addCropButton.setBounds(228, 358, 89, 23);
		panel.add(addCropButton);
		
		clearBtn = new JButton("Clear");
		clearBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				nameTextField.setText(null);
				weightTextField.setText(null);
				costTextField.setText(null);
				quantityTextField.setText(null);
				uploadedImage.setVisible(false);
			}
		});
		clearBtn.setFont(new Font("SansSerif", Font.PLAIN, 12));
		clearBtn.setForeground(new Color(255, 255, 255));
		clearBtn.setBackground(new Color(204, 0, 0));
		clearBtn.setBounds(15, 358, 89, 23);
		panel.add(clearBtn);
		
		return panel;
	}
	
	private JPanel configureUpdateCropPanel() {
		JPanel panel = new JPanel();
		panel.setVisible(false);
		panel.setBackground(new Color(204, 193, 255, 200));
		panel.setBounds(423, 257, 327, 392);
		panel.setLayout(null);
		
		JLabel lblUpdateCropDetails = new JLabel("UPDATE CROP DETAILS");
		lblUpdateCropDetails.setBounds(100, 11, 160, 14);
		lblUpdateCropDetails.setForeground(new Color(204, 0, 0));
		lblUpdateCropDetails.setFont(new Font("Rockwell Condensed", Font.PLAIN, 18));
		panel.add(lblUpdateCropDetails);
		
		JLabel lblSelectCrop = new JLabel("Select Crop:");
		lblSelectCrop.setFont(new Font("SansSerif", Font.BOLD, 12));
		lblSelectCrop.setBounds(44, 71, 69, 14);
		panel.add(lblSelectCrop);
		
		weightLabel = new JLabel("Weight:");
		weightLabel.setFont(new Font("SansSerif", Font.BOLD, 12));
		weightLabel.setBounds(44, 102, 46, 14);
		panel.add(weightLabel);
		
		JTextField weightTextFieldUpd = new JTextField();
		weightTextFieldUpd.setBounds(100, 100, 86, 20);
		weightTextFieldUpd.setColumns(10);
		panel.add(weightTextFieldUpd);
		
		lbsLabel = new JLabel("(lbs)");
		lbsLabel.setFont(new Font("SansSerif", Font.BOLD, 12));
		lbsLabel.setBounds(194, 102, 46, 14);
		panel.add(lbsLabel);
		
		costLabel = new JLabel("Cost:");
		costLabel.setFont(new Font("SansSerif", Font.BOLD, 12));
		costLabel.setBounds(44, 136, 46, 14);
		panel.add(costLabel);
		
		JTextField costTextFieldUpd = new JTextField();
		costTextFieldUpd.setBounds(100, 131, 86, 20);
		costTextFieldUpd.setColumns(10);
		panel.add(costTextFieldUpd);
		
		perUnitLabel = new JLabel("(per unit)");
		perUnitLabel.setFont(new Font("SansSerif", Font.BOLD, 12));
		perUnitLabel.setBounds(192, 136, 68, 14);
		panel.add(perUnitLabel);
		
		quantityAvailableLabel = new JLabel("Quantity available:");
		quantityAvailableLabel.setFont(new Font("SansSerif", Font.BOLD, 12));
		quantityAvailableLabel.setBounds(44, 173, 109, 14);
		panel.add(quantityAvailableLabel);
		
		JTextField quantityTextFieldUpd = new JTextField();
		quantityTextFieldUpd.setBounds(154, 171, 86, 20);
		quantityTextFieldUpd.setColumns(10);
		panel.add(quantityTextFieldUpd);
		
		uploadImageLabel = new JLabel("Upload image:");
		uploadImageLabel.setFont(new Font("SansSerif", Font.BOLD, 12));
		uploadImageLabel.setBounds(44, 232, 89, 14);
		panel.add(uploadImageLabel);
		
		for (Crop s : farmerDashboardViewModel.getFarmer().getCrops()) {
			list.addItem(s);
		}
		panel.add(list);
		
		list.setBounds(123, 69, 120, 20);
		
		Border border = BorderFactory.createLineBorder(Color.BLACK, 1);
		uploadedImage1 = new JLabel("");
		uploadedImage1.setBorder(border);
		uploadedImage1.setBounds(15, 252, 118, 95);
		panel.add(uploadedImage1);
		
		uploadImageBtn = new JButton("Click to upload image");
		uploadImageBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				JFileChooser fc = new JFileChooser(FileSystemView.getFileSystemView().getHomeDirectory());
				int rv = fc.showOpenDialog(null);
				
				if (rv == JFileChooser.APPROVE_OPTION) {
					try {
						BufferedImage img = ImageIO.read(fc.getSelectedFile());
						ResizeImage ri = new ResizeImage();
						Image img2 = ri.resImage(img);
						
						uploadedImage1.setIcon(new ImageIcon(img2));
						uploadedImage1.setVisible(true);
					} catch (IOException e) {
						logger.error("Unable to upload image.", e);
					}
					
				}
			}
		});
		uploadImageBtn.setBounds(143, 227, 160, 23);
		panel.add(uploadImageBtn);
		
		minimizeBtn = new JButton("-");
		minimizeBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				panel.setVisible(false);
			}
		});
		minimizeBtn.setBounds(281, 0, 46, 23);
		panel.add(minimizeBtn);
		
		JButton updateCropBtn = new JButton("Submit");
		updateCropBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				logger.info("Attempting to update crop");
				Crop selectedCrop = (Crop) list.getSelectedItem();
				
				Response response = cropController.update(new Crop(selectedCrop.getId(), selectedCrop.getName(), null, Float.parseFloat(weightTextFieldUpd.getText()), Integer.parseInt(quantityTextFieldUpd.getText()), Float.parseFloat(costTextFieldUpd.getText()), farmerDashboardViewModel.getFarmer().getId()));
				
				if (response.isSuccess()) {
					JOptionPane.showMessageDialog(null, "Crop successfully updated", "Successful", JOptionPane.INFORMATION_MESSAGE);
					;
					weightTextFieldUpd.setText(null);
					costTextFieldUpd.setText(null);
					quantityTextFieldUpd.setText(null);
					uploadedImage1.setVisible(false);
				}
			}
		});
		updateCropBtn.setFont(new Font("SansSerif", Font.PLAIN, 12));
		updateCropBtn.setBounds(228, 358, 89, 23);
		updateCropBtn.setForeground(Color.WHITE);
		updateCropBtn.setBackground(new Color(102, 204, 51));
		panel.add(updateCropBtn);
		
		clearBtn = new JButton("Clear");
		clearBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				weightTextFieldUpd.setText(null);
				costTextFieldUpd.setText(null);
				quantityTextFieldUpd.setText(null);
				uploadedImage1.setVisible(false);
				
			}
		});
		clearBtn.setFont(new Font("SansSerif", Font.PLAIN, 12));
		clearBtn.setBounds(10, 358, 89, 23);
		clearBtn.setForeground(Color.WHITE);
		clearBtn.setBackground(new Color(204, 0, 0));
		panel.add(clearBtn);
		
		return panel;
	}
	
	private JPanel configureViewCropsPanel() {
		JPanel panel_3 = new JPanel();
		panel_3.setVisible(false);
		panel_3.setLayout(null);
		panel_3.setBackground(new Color(204, 193, 255, 200));
		panel_3.setBounds(824, 53, 508, 290);
		
		JButton btnClickToView = new JButton("Click to view");
		btnClickToView.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Response response = cropController.get(farmerDashboardViewModel.getFarmer().getId());
				
				if (response.isSuccess()) {
					try {
						crops = (List<Crop>) response.getResult();
						ct.refreshData(crops);
						
						//tabViewCrops.setVisible(true);
					} catch (ClassCastException ex) {
						ex.printStackTrace();
					}
				}
				
			}
		});
		
		panel_3.add(ct.getTableHeader(), BorderLayout.NORTH);
		ct.setFillsViewportHeight(true);
		
		btnClickToView.setForeground(Color.WHITE);
		btnClickToView.setFont(new Font("SansSerif", Font.PLAIN, 12));
		btnClickToView.setBackground(new Color(102, 204, 51));
		btnClickToView.setBounds(194, 8, 160, 23);
		panel_3.add(btnClickToView);
		
		JLabel lblViewMyCrops = new JLabel("VIEW MY CROPS");
		lblViewMyCrops.setForeground(new Color(204, 0, 0));
		lblViewMyCrops.setFont(new Font("Rockwell Condensed", Font.PLAIN, 18));
		lblViewMyCrops.setBounds(21, 11, 160, 14);
		panel_3.add(lblViewMyCrops);
		
		JButton button_5 = new JButton("-");
		button_5.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				panel_3.setVisible(false);
			}
		});
		button_5.setBounds(462, 0, 46, 23);
		panel_3.add(button_5);

		//cropTable = new JTable(data,column);
		//cropTable.setBounds(0, 42, 508, 248);
		//panel_3.add(cropTable);
		return panel_3;
	}
	
	private JPanel configureViewCustomersPanel() {
		JPanel panel = new JPanel();
		panel.setVisible(false);
		panel.setLayout(null);
		panel.setBackground(new Color(204, 193, 255, 200));
		panel.setBounds(824, 350, 508, 301);
		
		customerForFarmerTable = new JTable(data, column);
		customerForFarmerTable.setLocation(0, 38);
		customerForFarmerTable.setSize(508, 263);
		panel.add(customerForFarmerTable);
		
		JButton btnClickToView_1 = new JButton("Click to view");
		btnClickToView_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnClickToView_1.setForeground(Color.WHITE);
		btnClickToView_1.setFont(new Font("SansSerif", Font.PLAIN, 12));
		btnClickToView_1.setBackground(new Color(102, 204, 51));
		btnClickToView_1.setBounds(202, 4, 188, 23);
		panel.add(btnClickToView_1);
		
		JLabel lblViewMyCustomers = new JLabel("VIEW MY CUSTOMERS");
		lblViewMyCustomers.setForeground(new Color(204, 0, 0));
		lblViewMyCustomers.setFont(new Font("Rockwell Condensed", Font.PLAIN, 18));
		lblViewMyCustomers.setBounds(10, 7, 175, 14);
		panel.add(lblViewMyCustomers);
		
		
		JButton button_6 = new JButton("-");
		button_6.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				panel.setVisible(false);
			}
		});
		button_6.setBounds(462, 0, 46, 23);
		panel.add(button_6);
		return panel;
	}
}