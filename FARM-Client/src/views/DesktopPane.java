package views;

import controllers.AuthController;
import controllers.CropController;
import models.*;
import resources.Response;
import viewmodels.CustomerDashboardViewModel;
import viewmodels.FarmerDashboardViewModel;
import viewmodels.Login;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.filechooser.FileSystemView;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

public class DesktopPane extends JFrame {
	
	private final String PHOTO_DIR = System.getProperty("user.dir") + File.separator + "FARM-Client" + File.separator + "src" + File.separator + "images" + File.separator;

	boolean checkP;
	boolean checkA;
	boolean checkN;
	boolean checkI;
	boolean checkE;
	private byte[] profilepic;
	private String filename;
	private JDesktopPane desktopPane;
	private JInternalFrame frame;
	private JPanel registerPanel;
	private AuthController authController;
	
	public DesktopPane(AuthController authController) {
		super("Farmer's Market Main");
		
		this.authController = authController;
		
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setExtendedState(JFrame.MAXIMIZED_BOTH);
		setLayout(new BorderLayout());
		desktopPane = new JDesktopPane();
		
		configureLoginView();
		
		configureRegisterView();
		
		add(desktopPane, BorderLayout.CENTER);
	}
	
	public void configureLoginView(){
		
		JPanel panel = new JPanel();
		
		Dimension dim = getToolkit().getScreenSize();
		panel.setSize(1000,500);
		panel.setLocation(dim.width/2 - panel.getWidth()/2,       dim.height/2 - panel.getHeight()/2);
		
		panel.setBackground(new Color(204, 255, 153));
		panel.setBorder(new EmptyBorder(5, 5, 5, 5));
		
		panel.setLayout(null);
		
		JLabel mainLabel = new JLabel("Farmers Market");
		mainLabel.setBounds(406, 0, 200, 55);
		mainLabel.setFont(new Font("Sneakerhead BTN Condensed", Font.BOLD, 25));
		mainLabel.setForeground(new Color(218, 34, 34));
		panel.add(mainLabel);
		
		JRadioButton farmerRadioButton = new JRadioButton("Farmer");
		farmerRadioButton.setBounds(370, 45, 109, 23);
		
		JRadioButton customerRadioButton = new JRadioButton("Customer");
		customerRadioButton.setBounds(500, 45, 109, 23);
		
		ButtonGroup userTypeButtonGroup = new ButtonGroup();
		userTypeButtonGroup.add(farmerRadioButton);
		userTypeButtonGroup.add(customerRadioButton);
		
		panel.add(farmerRadioButton);
		panel.add(customerRadioButton);
		
		JLabel imageLabel = new JLabel();
		imageLabel.setBounds(-160, 0, 493, 500);
		imageLabel.setIcon(new ImageIcon(DesktopPane.class.getResource("/images/f1.jpg")));
		panel.add(imageLabel);
		
		JLabel usernameLabel = new JLabel("Username");
		usernameLabel.setForeground(Color.BLACK);
//		usernameLabel.setHorizontalAlignment(SwingConstants.CENTER);
		usernameLabel.setBounds(372, 70, 227, 34);
		panel.add(usernameLabel);
		
		JTextField usernameField = new JTextField(10);
		usernameField.setBounds(372, 110, 227, 34);
		panel.add(usernameField);
		
		JLabel lblPassword = new JLabel("Password");
		lblPassword.setForeground(Color.BLACK);
//		lblPassword.setHorizontalAlignment(SwingConstants.CENTER);
		lblPassword.setBounds(372, 150, 227, 34);
		panel.add(lblPassword);
		
		JPasswordField passwordField = new JPasswordField(10);
		passwordField.setBounds(372, 190, 227, 34);
		panel.add(passwordField);
		
		JButton btnLogin = new JButton("Login");
		btnLogin.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (farmerRadioButton.isSelected()) {
					//binaryImage();
					if(usernameField.getText().equals("") | passwordField.getText().equals(""))
					{
						JOptionPane.showMessageDialog(null,"Either the username or password is incorrect");
					}
					else{
						Response response = authController.login(new Login(usernameField.getText(), new String(passwordField.getPassword()), UserType.FARMER));

						if (response.isSuccess()) {
							JOptionPane.showMessageDialog(frame,"Login Successful");
							frame = new JInternalFrame("Farmer Dashboard", true, true, true, true);

							Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
							frame.setSize(screenSize);
							FarmerDashboard farmerDashboard = new FarmerDashboard(new CropController(new FarmerClient()), (FarmerDashboardViewModel) response.getResult());// create new panel
							frame.add(farmerDashboard, BorderLayout.CENTER); // add panel
							frame.pack(); // set internal frame to size of contents
							desktopPane.add(frame); // attach internal frame
							frame.setVisible(true);
							userTypeButtonGroup.clearSelection();
							usernameField.setText("");
							passwordField.setText("");
						} else {
							JOptionPane.showMessageDialog(null, response.getErrorMessage(), "Authentication Failed", JOptionPane.ERROR_MESSAGE);
						}

					}

				} else if (customerRadioButton.isSelected()) {
					if(usernameField.getText().equals("") | passwordField.getText().equals(""))
					{
						JOptionPane.showMessageDialog(null,"Either the username or password is incorrect");
					}
					else
					{
						Response response = authController.login(new Login(usernameField.getText(), new String(passwordField.getPassword()), UserType.CUSTOMER));

						if (response.isSuccess()) {
							JOptionPane.showMessageDialog(frame,"Login Successful");
							frame = new JInternalFrame("Customer Dashboard", true, true, true, true);

							Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
							frame.setSize(screenSize);

							CustomerDashboard customerDashboard = new CustomerDashboard((CustomerDashboardViewModel) response.getResult());// create new panel
							frame.add(customerDashboard, BorderLayout.CENTER); // add panel
							frame.pack(); // set internal frame to size of contents
							desktopPane.add(frame); // attach internal frame
							frame.setVisible(true);
							userTypeButtonGroup.clearSelection();
							usernameField.setText("");
							passwordField.setText("");
						} else {
							JOptionPane.showMessageDialog(null, response.getErrorMessage(), "Authentication Failed", JOptionPane.ERROR_MESSAGE);

						}

					}

				} else {
					
					JOptionPane.showMessageDialog(null, "Please select user type", "Authentication Failed", JOptionPane.ERROR_MESSAGE);
					
				}
			}
		});
		btnLogin.setBackground(new Color(147, 163, 11));
		btnLogin.setForeground(Color.WHITE);
		btnLogin.setBounds(406, 250, 178, 25);
		panel.add(btnLogin);
		
		JButton btnRegister = new JButton("Sign Up");
		btnRegister.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				frame = new JInternalFrame("Register", true, true, true, true);
				frame.add(registerPanel, BorderLayout.CENTER); // add panel
				frame.pack(); // set internal frame to size of contents
				desktopPane.add(frame); // attach internal frame
				frame.setVisible(true);
			}
			
		});
		
		btnRegister.setBackground(new Color(218, 34, 34));
		btnRegister.setForeground(Color.WHITE);
		btnRegister.setBounds(406, 290, 178, 25);
		panel.add(btnRegister);
		
		desktopPane.add(panel);
	}
	
	private void configureRegisterView() {
		registerPanel = new JPanel();
		registerPanel.setBounds(100, 100, 1357, 650);
		registerPanel.setBackground(new Color(204, 255, 153));
		registerPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		registerPanel.setLayout(null);
		
		JLabel lblRegistration = new JLabel("Sign up");
		lblRegistration.setBackground(Color.BLACK);
		lblRegistration.setForeground(new Color(218, 34, 34));
		lblRegistration.setHorizontalAlignment(SwingConstants.CENTER);
		lblRegistration.setFont(new Font("Sneakerhead BTN Condensed", Font.BOLD, 32));
		lblRegistration.setBounds(313, 11, 598, 35);
		registerPanel.add(lblRegistration);
		
		JPanel panel1 = new JPanel();
		panel1.setBackground(new Color(204, 255, 153));
		panel1.setBounds(0, 0, 303, 571);
		registerPanel.add(panel1);
		panel1.setLayout(null);
		
		JLabel lblNewLabel_1 = new JLabel("");
		lblNewLabel_1.setIcon(new ImageIcon(DesktopPane.class.getResource("/images/f4.jpg")));
		lblNewLabel_1.setBounds(-108, -61, 411, 621);
		panel1.add(lblNewLabel_1);
		
		JRadioButton rdbtnFarmer = new JRadioButton("Farmer");
		
		ButtonGroup buttonGroup = new ButtonGroup();
		buttonGroup.add(rdbtnFarmer);
		rdbtnFarmer.setBounds(532, 53, 109, 23);
		registerPanel.add(rdbtnFarmer);
		
		JRadioButton rdbtnCustomer = new JRadioButton("Customer");
		
		buttonGroup.add(rdbtnCustomer);
		rdbtnCustomer.setBounds(653, 53, 109, 23);
		registerPanel.add(rdbtnCustomer);
		
		JLabel lblUserType = new JLabel("User type");
		lblUserType.setBounds(465, 57, 62, 14);
		registerPanel.add(lblUserType);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBackground(new Color(144, 238, 144));
		panel_1.setBounds(313, 87, 598, 398);
		registerPanel.add(panel_1);
		panel_1.setLayout(null);
		panel_1.setVisible(false);
		
		JTextField emailTextField = new JTextField(10);
		emailTextField.setBounds(77, 11, 229, 26);
		panel_1.add(emailTextField);
		JLabel lblEmail = new JLabel("Email");
		lblEmail.setBounds(10, 23, 46, 14);
		panel_1.add(lblEmail);
		
		JTextField firstnameTextField = new JTextField(10);
		firstnameTextField.setBounds(97, 56, 209, 26);
		panel_1.add(firstnameTextField);
		
		JTextField lastnameTextField = new JTextField(10);
		lastnameTextField.setBounds(97, 109, 209, 26);
		panel_1.add(lastnameTextField);
		
		JLabel lblFirstName = new JLabel("First name");
		lblFirstName.setBounds(10, 68, 66, 14);
		panel_1.add(lblFirstName);
		
		JLabel lblLastName = new JLabel("Last name");
		lblLastName.setBounds(10, 121, 66, 14);
		panel_1.add(lblLastName);
		
		JLabel lblNewLabel1 = new JLabel("Password");
		lblNewLabel1.setBounds(10, 173, 77, 14);
		panel_1.add(lblNewLabel1);
		
		JLabel lblVerifyPassword = new JLabel("Verify password");
		lblVerifyPassword.setBounds(10, 226, 99, 14);
		panel_1.add(lblVerifyPassword);
		
		JPasswordField passwordTextField = new JPasswordField();
		passwordTextField.setBounds(97, 161, 209, 26);
		panel_1.add(passwordTextField);
		
		JPasswordField confirmPasswordTextField = new JPasswordField();
		confirmPasswordTextField.setBounds(107, 214, 199, 26);
		panel_1.add(confirmPasswordTextField);
		
		JPanel panel_2 = new JPanel();
		panel_2.setBackground(new Color(204, 255, 153));
		panel_2.setBounds(10, 261, 301, 115);
		panel_1.add(panel_2);
		panel_2.setLayout(null);
		panel_2.setVisible(false);
		
		JTextField streetTextField = new JTextField(10);
		streetTextField.setBounds(87, 22, 193, 20);
		panel_2.add(streetTextField);
		
		JTextField townTextField = new JTextField(10);
		townTextField.setBounds(87, 53, 193, 20);
		panel_2.add(townTextField);
		
		JTextField parishTextField = new JTextField(10);
		parishTextField.setBounds(87, 84, 193, 20);
		panel_2.add(parishTextField);
		
		JLabel lblAddress = new JLabel("Farm Address");
		lblAddress.setHorizontalAlignment(SwingConstants.CENTER);
		lblAddress.setBounds(10, 0, 281, 14);
		panel_2.add(lblAddress);
		
		JLabel lblStreet = new JLabel("Street");
		lblStreet.setBounds(10, 28, 46, 14);
		panel_2.add(lblStreet);
		
		JLabel lblTown = new JLabel("Town");
		lblTown.setBounds(10, 56, 46, 14);
		panel_2.add(lblTown);
		
		JLabel lblParish = new JLabel("Parish");
		lblParish.setBounds(10, 87, 46, 14);
		panel_2.add(lblParish);
		
		JPanel panel_3 = new JPanel();
		panel_3.setBackground(new Color(204, 255, 153));
		panel_3.setBounds(340, 11, 248, 365);
		panel_1.add(panel_3);
		panel_3.setLayout(null);
		panel_3.setVisible(false);
		
		Border border = BorderFactory.createLineBorder(Color.BLACK, 1);
		JLabel lbPicture = new JLabel("");
		lbPicture.setBorder(border);
		lbPicture.setBounds(35, 24, 180, 145);
		panel_3.add(lbPicture);
		
		
		JLabel lblProfilePicture = new JLabel("Profile Picture");
		lblProfilePicture.setHorizontalAlignment(SwingConstants.CENTER);
		lblProfilePicture.setBounds(10, -1, 228, 14);
		panel_3.add(lblProfilePicture);
		
		JButton btnUpload = new JButton("Upload");
		btnUpload.setBackground(new Color(147, 163, 11));
		btnUpload.setForeground(Color.WHITE);
		btnUpload.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JFileChooser fc = new JFileChooser(FileSystemView.getFileSystemView().getHomeDirectory());
				int rv = fc.showOpenDialog(null);
				
				if (rv == JFileChooser.APPROVE_OPTION) {
					try {

						File f = fc.getSelectedFile();
						BufferedImage img = ImageIO.read(f);
						ResizeImage ri = new ResizeImage();
						Image img2 = ri.resImage2(img);
						filename = f.getAbsolutePath();
						lbPicture.setIcon(new ImageIcon(img2));
						lbPicture.setVisible(true);
					} catch (IOException ex) {
						// TODO Auto-generated catch block
						ex.printStackTrace();
					}
					
				}
				
			}
		});

		btnUpload.setBounds(87, 173, 89, 23);
		panel_3.add(btnUpload);

		JButton btnNewButton = new JButton("Submit");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				if (rdbtnCustomer.isSelected()) {
					 checkP = checkBothPassword();
					 checkI = checkImage();
					 checkN = checkName();
					 checkE = checkEmail();
					 if(checkP==true && checkN==true && checkI==true && checkE==true)
					 {
						 binaryImage();
						 Response response = authController.registerCustomer(new Customer(emailTextField.getText(), new String(passwordTextField.getPassword()), firstnameTextField.getText(), lastnameTextField.getText(), profilepic));

						 if (response.isSuccess()) {
						 	JOptionPane.showMessageDialog(frame,"New Customer registered");
						 	defaultData();
						 	frame.dispose();
							 /*
							 frame = new JInternalFrame("Customer Dashboard", true, true, true, true);
							 CustomerDashboard customerDashboard = new CustomerDashboard((CustomerDashboardViewModel) response.getResult());// create new panel
							 frame.add(customerDashboard, BorderLayout.CENTER); // add panel
							 frame.pack(); // set internal frame to size of contents
							 desktopPane.add(frame); // attach internal frame
							 frame.setVisible(true);
							 */
						 }

					 }
					 else{
						 JOptionPane.showMessageDialog(null,"Error in signing up","Unsuccessful",JOptionPane.WARNING_MESSAGE);
					 }

					
				} else if (rdbtnFarmer.isSelected()) {

					checkP = checkBothPassword();
					checkA= checkAddress();
					checkI = checkImage();
					checkN = checkName();
					checkE = checkEmail();
					if(checkP==true && checkN==true && checkI==true && checkE==true)
					{
						binaryImage();
						Response response = authController.register(new Farmer(emailTextField.getText(), new String(passwordTextField.getPassword()), firstnameTextField.getText(), lastnameTextField.getText(), profilepic, new Address(streetTextField.getText(), townTextField.getText(), parishTextField.getText())));

						if (response.isSuccess()) {
							JOptionPane.showMessageDialog(frame,"New Farmer registered");
							defaultData();
							frame.dispose();

							/*
							frame = new JInternalFrame("Farmer Dashboard", true, true, true, true);
							FarmerDashboard farmerDashboard = new FarmerDashboard(new CropController(new FarmerClient()), (FarmerDashboardViewModel) response.getResult());// create new panel
							frame.add(farmerDashboard, BorderLayout.CENTER); // add panel
							frame.pack(); // set internal frame to size of contents
							desktopPane.add(frame); // attach internal frame
							frame.setVisible(true);
							*/
						}

					}

					
					else{
						JOptionPane.showMessageDialog(null,"Error in signing up","Unsuccessful",JOptionPane.WARNING_MESSAGE);
					}

				}
				else{
					JOptionPane.showMessageDialog(null,"Please select a user type","User type error",JOptionPane.ERROR_MESSAGE);
				}
			}
			private boolean checkEmail()
			{
				if(emailTextField.getText().equals(""))
				{
					JOptionPane.showMessageDialog(null,"Please enter your email address","Email error",JOptionPane.ERROR_MESSAGE);
					return false;
				}
				else
				{
					return true;
				}
			}
			private boolean checkBothPassword(){
				if(passwordTextField.getText().equals(confirmPasswordTextField.getText()))
				{
					return true;
				}
				else
				{
					JOptionPane.showMessageDialog(null,"Please ensure both passwords are the same","Password error",JOptionPane.ERROR_MESSAGE);
					return false;
				}
			}
			private boolean checkName()
			{
				if(firstnameTextField.getText().equals("") | lastnameTextField.getText().equals(""))
				{
					JOptionPane.showMessageDialog(null,"Please enter both first and last names","Name error",JOptionPane.ERROR_MESSAGE);
					return false;
				}
				else
				{
					return true;
				}
			}
			private boolean checkAddress()
			{
				if(streetTextField.getText().equals("") | townTextField.getText().equals("") | parishTextField.getText().equals(""))
				{
					JOptionPane.showMessageDialog(null,"Please fill out all address fields","address error",JOptionPane.ERROR_MESSAGE);
					return false;
				}
				else
				{
					return true;
				}
			}
			private boolean checkImage(){
				if(filename.equals(""))
				{
					JOptionPane.showMessageDialog(null,"Please select an image","Image error",JOptionPane.ERROR_MESSAGE);
					return false;
				}
				return true;
			}
			private void defaultData(){
				emailTextField.setText(null);
				firstnameTextField.setText(null);
				lastnameTextField.setText(null);
				passwordTextField.setText(null);
				confirmPasswordTextField.setText(null);
				streetTextField.setText(null);
				townTextField.setText(null);
				parishTextField.setText(null);
				lbPicture.setVisible(false);
				panel_1.setVisible(false);
				filename="";
				buttonGroup.clearSelection();

			}
		});
		btnNewButton.setForeground(Color.WHITE);
		btnNewButton.setBackground(new Color(147, 163, 11));
		btnNewButton.setBounds(822, 496, 89, 23);
		registerPanel.add(btnNewButton);
		
		JButton btnNewButton_1 = new JButton("Cancel");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				emailTextField.setText(null);
				firstnameTextField.setText(null);
				lastnameTextField.setText(null);
				passwordTextField.setText(null);
				confirmPasswordTextField.setText(null);
				streetTextField.setText(null);
				townTextField.setText(null);
				parishTextField.setText(null);
				lbPicture.setVisible(false);
				panel_1.setVisible(false);
				filename="";
				buttonGroup.clearSelection();
				
			}
		});
		btnNewButton_1.setForeground(Color.WHITE);
		btnNewButton_1.setBackground(new Color(218, 34, 34));
		btnNewButton_1.setBounds(705, 496, 89, 23);
		registerPanel.add(btnNewButton_1);
		
		JButton btnBack = new JButton("Back");
		btnBack.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frame.dispose();
			}
		});
		btnBack.setForeground(Color.WHITE);
		btnBack.setBackground(new Color(218, 34, 34));
		btnBack.setBounds(590, 496, 89, 23);
		registerPanel.add(btnBack);
		
		rdbtnFarmer.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				panel_1.setVisible(true);
				panel_2.setVisible(true);
				panel_3.setVisible(true);
			}
		});
		rdbtnCustomer.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				panel_1.setVisible(true);
				panel_2.setVisible(false);
				panel_3.setVisible(true);
			}
		});
	}

	private byte[] binaryImage() {

			try{
				File photo = new File(filename);
				FileInputStream fis = new FileInputStream(photo);

				ByteArrayOutputStream os = new ByteArrayOutputStream();
				byte[] buff = new byte[1024];
				for(int i; (i=fis.read(buff)) !=-1;){
					os.write(buff,0,i);
				}
				profilepic=os.toByteArray();
				return profilepic;
			}catch(IOException ex)
			{
				ex.printStackTrace();
			}
			return null;


	}
}
