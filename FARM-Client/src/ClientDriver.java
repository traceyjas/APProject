import controllers.AuthController;
import models.Client;
import views.DesktopPane;

import java.awt.*;

public class ClientDriver {
	
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					DesktopPane frame = new DesktopPane(new AuthController(new Client()));
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
}
