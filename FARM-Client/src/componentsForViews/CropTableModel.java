package componentsForViews;

import models.Crop;

import javax.swing.*;
import javax.swing.table.AbstractTableModel;
import java.util.ArrayList;
import java.util.List;

public class CropTableModel extends AbstractTableModel{

    private List<Crop> crops;
    public static final String[] COLUMN_NAMES = {"ID", "CropName", "Cost", "Quantity", "Weight", "Available", "Photo" };

    public CropTableModel(){
        this(new ArrayList<Crop>());
    }

    public CropTableModel(List<Crop> cropData){
        crops = new ArrayList<Crop>();
        for(Crop c : cropData){
            crops.add(c);
        }
    }

    public List<Crop> getCrops(){
        return crops;
    }

    @Override
    public int getRowCount() {
        return crops.size();
    }

    @Override
    public int getColumnCount() {
        return COLUMN_NAMES.length;
    }

    @Override
    public Object getValueAt(int rowIndex, int colIndex) {
        return crops.get(rowIndex);//recheck this
    }
}
