package componentsForViews;

import models.Crop;

import javax.swing.*;
import java.util.ArrayList;
import java.util.List;

public class CropTable extends JTable {

    public CropTable(){
        this(null);
    }

    public CropTable(List<Crop> crops){
        super(new CropTableModel(crops == null ? new ArrayList<Crop>() : crops));

        initTable(crops);
    }

    private void initTable(List<Crop> crops){
        for(int i=0; i < CropTableModel.COLUMN_NAMES.length; i++){
            getColumnModel().getColumn(i).setHeaderValue(CropTableModel.COLUMN_NAMES[i]);
        }
    }

    public void refreshData(List<Crop> crops){
        if(crops == null){
            crops = new ArrayList<Crop>();
        }

        this.dataModel = new CropTableModel(crops);
        this.repaint();
    }


}
