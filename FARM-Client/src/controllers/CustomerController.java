package controllers;

import models.CustomerClient;
import models.Customer;
import resources.Response;

public class CustomerController{
	private CustomerClient customerClient;
	
	public CustomerController(CustomerClient customerClient) {
		this.customerClient = customerClient;
	}
	
	public Response addshoppingcart(Customer customer,ShoppingItem item) 
	{
		return customerClient.addToShoppingCart(customer, item); 
	}
	

	public Response updateBalance(Customer customer) {
		return customerClient.updateAccountBalance(customer);    //AddCrop(crop);
	}

	public Response getAllCrops(){
		return customerClient.getCrops();
	}
	
	public Response makePurchase(Customer customer){
		return customerClient.makePurchase();
	}
}
