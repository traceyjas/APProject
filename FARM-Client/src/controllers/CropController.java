package controllers;

import models.Crop;
import models.FarmerClient;
import resources.Response;

public class CropController{
	
	private FarmerClient farmerClient;
	
	public CropController(FarmerClient farmerClient) {
		this.farmerClient = farmerClient;
	}

	public Response create(Crop crop) {
		return farmerClient.AddCrop(crop);
	}
	
	public Response update(Crop crop) {
		return farmerClient.UpdateCrop(crop);
	}
	
	public Response get(int farmerId){
		return farmerClient.getCrops(farmerId);
	}
}
