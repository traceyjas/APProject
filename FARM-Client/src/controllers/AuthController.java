package controllers;

import resources.Response;
import viewmodels.Login;
import models.*;

public class AuthController {

    private Client client;

    public AuthController(Client client) {
        this.client = client;
    }

    public Response login(Login login){
    	
    	Response response = client.signIn(login);
    	
		return response;
    }
    
    public Response register(Farmer farmer){
    	
    	Response response = client.register(farmer);
	
		return response;
	}
    
    public Response registerCustomer(Customer customer){
	
		Response response = client.registerCustomer(customer);
	
		return response;
	}

}
