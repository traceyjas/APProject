package controllers;

import models.FarmerClient;
import resources.Response;

public class FarmerController {
	
	private FarmerClient farmerClient;
	
	public FarmerController(FarmerClient farmerClient) {
		this.farmerClient = farmerClient;
	}
	
	public Response searchFarmers(String first, String last) {
		return farmerClient.searchforFarmer(first,last);    //AddCrop(crop);
	}
	
	public Response getAllFarmers(){
		return farmerClient.getAllFarmers();
	}

}
