package models;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import resources.Request;
import resources.Response;
import models.ShoppingItem;
import java.io.IOException;

public class CustomerClient extends Client {
	
	private Logger logger = LogManager.getLogger(CustomerClient.class);
	
	public CustomerClient() {
		super();
	}
	
	public Response updateAccountBalance(Customer customer) {
		try {
			logger.info("Sending UPDATEACCOUNTBALANCE request to server");
			oos.writeObject(new Request("UPDATEACCOUNTBALANCE", customer));
			
			logger.info("Receiving UPDATEACCOUNTBALANCE response from server");
			response = (Response) ois.readObject();
			oos.flush();
		} catch (IOException e) {
			logger.error("Could not send UPDATEACCOUNTBALANCE request to server.", e);
		} catch (ClassNotFoundException e) {
			logger.error("Could not receive UPDATEACCOUNTBALANCE response from server.", e);
		}
		
		return response;
	}
	
	public Response getCrops() {
		try {
			oos.writeObject(new Request("SELECTALLCROPFORCUSTOMER"));
	
	public Response addToShoppingCart(Customer customer,ShoppingItem item){	
		try{
			oos.writeObject(new Request("ADDTOCART", customer, item));
			response = (Response) ois.readObject();
			oos.flush();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		
		return response;
	}
	
	
	public Response makePurchase() {
		try {
			logger.info("Sending UPDATEACCOUNTBALANCE request to server");
			oos.writeObject(new Request("UPDATEACCOUNTBALANCE", customer));
			
			logger.info("Receiving UPDATEACCOUNTBALANCE response from server");
	
	public Response getCrops() {
		try{
			oos.writeObject(new Request("SELECTALLCROPFORCUSTOMER"));
			response = (Response) ois.readObject();
			oos.flush();
		} catch (IOException e) {
			logger.error("Could not send UPDATEACCOUNTBALANCE request to server.", e);
		} catch (ClassNotFoundException e) {
			logger.error("Could not receive UPDATEACCOUNTBALANCE response from server.", e);
		}
		
		return response;
	}
}
