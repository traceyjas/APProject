package models;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import resources.Request;
import resources.Response;
import viewmodels.Login;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetAddress;
import java.net.Socket;

public class Client {
	
	protected ObjectOutputStream oos;
	protected ObjectInputStream ois;
	private Socket socket;
	private Logger logger = LogManager.getLogger(Client.class);
	protected Response response;
	
	public Client(){
		createConnection();
	}
	
	public boolean createConnection(){
		closeConnection();
		
		try{
			socket = new Socket(InetAddress.getLocalHost(),9000);
			logger.info("CLIENT LOG:Client Connected to server on port 9000");
			initConnectionStreams();
			return true;
		}catch(IOException e){
			e.printStackTrace();
			closeConnection();
		}
		return false;
	}
	
	private void initConnectionStreams() throws IOException {
		if (socket != null && socket.isConnected()) {
			oos = new ObjectOutputStream(socket.getOutputStream());
			ois = new ObjectInputStream(socket.getInputStream());
		}
	}
	
	private void closeConnectionStreams() throws IOException {
		if(oos != null)oos.close();
		if(ois != null)ois.close();
	}
	
	public void closeConnection() {
		if(socket != null){
			try {
				closeConnectionStreams();
				socket.close();
				socket = null;
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	public boolean isConnected(){
		//Discuss TCP constraints here
		return socket != null && socket.isConnected();
	}
	
	public Response signIn(Login login){
		try {
			if (login.getUserType().equals(UserType.CUSTOMER)){
				oos.writeObject(new Request("LOGINCUSTOMER", login));
			} else if (login.getUserType().equals(UserType.FARMER)){
				oos.writeObject(new Request("LOGINFARMER", login));
			}
			response = (Response) ois.readObject();
			oos.flush();
		} catch (IOException | ClassCastException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		
		return response;
	}
	
	public Response register(Farmer farmer){
		try{
			oos.writeObject(new Request("REGISTERFARMER", farmer));
			response = (Response) ois.readObject();
			oos.flush();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		
		return response;
	}
	
	public Response registerCustomer(Customer customer){
		try{
			oos.writeObject(new Request("REGISTERCUSTOMER", customer));
			response = (Response) ois.readObject();
			oos.flush();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		
		return response;
	}
}
