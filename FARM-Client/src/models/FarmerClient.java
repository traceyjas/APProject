package models;

import resources.Request;
import resources.Response;

import java.io.IOException;

public class FarmerClient extends Client{
	
	public FarmerClient() {
		super();
	}
	
	public Response AddCrop(Crop crop){
		try{
			oos.writeObject(new Request("ADDCROP", crop));
			response = (Response) ois.readObject();
			oos.flush();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		
		return response;
	}
	
	public Response UpdateCrop(Crop crop){
		try{
			oos.writeObject(new Request("UPDATECROP", crop));
			response = (Response) ois.readObject();
			oos.flush();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		
		return response;
	}
	
	public Response getCrops(int farmerId) {
		try{
			oos.writeObject(new Request("SELECTALLCROP", farmerId));
			response = (Response) ois.readObject();
			oos.flush();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		
		return response;
	}
	
	public Response searchforFarmer(String first, String last){		//Shevar's Function
		String arr[] = {first,last};
		try{
			oos.writeObject(new Request("FARMERSEARCH",arr));
			response = (Response) ois.readObject();
			oos.flush();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		
		return response;
	}
	

	public Response getAllFarmers() {			//Shevar's Function
		try{
			oos.writeObject(new Request("SELECTALLFARMERS"));
			response = (Response) ois.readObject();
			oos.flush();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		
		return response;
	}
}
