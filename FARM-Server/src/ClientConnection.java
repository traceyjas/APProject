import database.CropProvider;
import database.CustomerProvider;
import database.FarmerProvider;
import database.CartProvider;
import models.Crop;
import models.Customer;
import models.Farmer;
import models.ShoppingCart;
import models.ShoppingItem;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import resources.Request;
import resources.Response;
import viewmodels.CustomerDashboardViewModel;
import viewmodels.FarmerCustomer;
import viewmodels.FarmerDashboardViewModel;
import viewmodels.Login;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.net.Socket;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.List;

public class ClientConnection implements Runnable {
	
	private Socket socket;
	private ObjectOutputStream oos;
	private ObjectInputStream ois;
	private String username;
	private ClientManager clientManager;
	
	private static Logger logger = LogManager.getLogger(ClientConnection.class);
	
	public ClientConnection(String username, Socket conn) {
		this.username = username;
		clientManager = ClientManager.getInstance();
		socket = conn;
		if (initStreams()) {
			Thread myThread = new Thread(this);
			myThread.start();
		}
		
	}
	
	private boolean initStreams() {
		try {
			if (socket == null)
				return false;
			
			oos = new ObjectOutputStream(socket.getOutputStream());
			ois = new ObjectInputStream(socket.getInputStream());
			return true;
		} catch (IOException e) {
			e.printStackTrace();
		}
		return false;
	}
	
	private void close() {
		try {
			oos.close();
			ois.close();
			socket.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void writeObjectToStream(Serializable object) throws IOException {
		try {
			oos.writeObject(object);
		} catch (SocketException e) {
			if (e.getMessage().equals("Socket closed")) {
				close();
				clientManager.removeClient(username);
			} else {
				throw e;
			}
		}
	}
	
	public String getUsername() {
		return username;
	}
	
	public void setUsername(String username) {
		this.username = username;
	}
	
	@SuppressWarnings({"rawtypes","unchecked"})
	@Override
	public void run() {
		
		String action = "";
		try {
			while (!action.equals("EXIT")) {
				
				Request request = (Request) ois.readObject();
				action = request.getAction();
				Response response = new Response(null);
				
				switch (action) {
					case "REGISTERFARMER": {
						logger.info("Request: " + action);
						Farmer farmer = (Farmer) request.getData();
						FarmerProvider farmerProvider = new FarmerProvider();
						int success = farmerProvider.add(farmer);
						
						if (success == 1) {
							FarmerDashboardViewModel farmerDashboardViewModel = new FarmerDashboardViewModel(farmer, new ArrayList<>());
							response = new Response(farmerDashboardViewModel);
						} else
							response = new Response(farmer, false, "Unable to register farmer");
					}
					break;
					case "LOGINFARMER": {
						logger.info("Request: " + action);
						Login login = (Login) request.getData();
						FarmerProvider farmerProvider = new FarmerProvider();
						Farmer farmer = farmerProvider.login(login.getUsername(), login.getPassword());
						
						if (farmer != null) {
							List<FarmerCustomer> farmerCustomers = farmerProvider.getCustomers(farmer.getId());
							List<Crop> crops = farmerProvider.selectCrops(farmer.getId());
							farmer.setCrops(crops);
							
							FarmerDashboardViewModel farmerDashboardViewModel = new FarmerDashboardViewModel(farmer, farmerCustomers);
							
							response = new Response(farmerDashboardViewModel);
						} else
							response = new Response(login, false, "Invalid Credentials");
					}
					break;
					case "REGISTERCUSTOMER": {
						logger.info("Request: " + action);
						CustomerProvider customerProvider = new CustomerProvider();
						Customer customer = (Customer) request.getData();
						int success = customerProvider.add(customer);
						
						if (success == 1) {
							CustomerDashboardViewModel customerDashboardViewModel = new CustomerDashboardViewModel(customer);
							response = new Response(customerDashboardViewModel);
						} else
							response = new Response(customer, false, "Unable to register customer");
					}
					break;
					
					case "UPDATEACCOUNTBALANCE": {
						logger.info("Request: " + action);
						Customer customer = (Customer) request.getData();
						CustomerProvider customerProvider = new CustomerProvider();
						int success = customerProvider.update(customer, customer.getId());
						
						if (success != 1)
							response = new Response(customer, false, "Unable to update customer (balance)");
						else
							response = new Response(customer);
					}
					
					break;
					
					case "FARMERSEARCH": {
						logger.info("Request: " + action);
						String array[] = (String[]) request.getData();
						FarmerProvider farmerProvider = new FarmerProvider();
						List <Farmer> farmers = farmerProvider.searchAll(array[0], array[1]);
						
						if (farmers != null) {
							
							response = new Response(farmers);
						} else
							response = new Response(farmers, false, "Farmer Search Unsuccessful");
					}
					break;
					
					case "SELECTALLFARMERS": {
						logger.info("Request: " + action);
						FarmerProvider fp = new FarmerProvider();
						List<Farmer> farmers = fp.selectAll();
						
						if(farmers != null)
							response = new Response(farmers);
						else
							response = new Response(farmers, false, "Unsuccessful farmer search");
					}
					break;
					case "LOGINCUSTOMER": {
						logger.info("Request: " + action);
						Login login = (Login) request.getData();
						CustomerProvider customerProvider = new CustomerProvider();
						Customer customer = customerProvider.login(login.getUsername(), login.getPassword());
						
						if (customer != null) {
							CustomerDashboardViewModel customerDashboardViewModel = new CustomerDashboardViewModel(customer);
							
							response = new Response(customerDashboardViewModel);
						} else
							response = new Response(login, false, "Invalid Credentials");
					}
					break;
					
					case "ADDCROP": {
						logger.info("Request: " + action);
						Crop crop = (Crop) request.getData();
						CropProvider cropProvider = new CropProvider();
						int success = cropProvider.add(crop);
						
						if (success != 1)
							response = new Response(crop, false, "Unable to create crop");
						else
							response = new Response(crop);
					}
					break;
					case "UPDATECROP": {
						logger.info("Request: " + action);
						Crop crop = (Crop) request.getData();
						CropProvider cropProvider = new CropProvider();
						int success = cropProvider.update(crop, crop.getId());
						
						if (success != 1)
							response = new Response(crop, false, "Unable to update crop");
						else
							response = new Response(crop);
					}
					break;
					
					case "SELECTCROP": {
						logger.info("Request: " + action);
						int id = (int) request.getData();
						CropProvider cropProvider = new CropProvider();
						Crop crop = cropProvider.get(id);
						
						if (crop == null)
							response = new Response(crop, false, "Unable to retrieve crop");
						else
							response = new Response(crop);
					}
					break;
					case "SELECTALLCROP": {
						logger.info("Request: " + action);
						int farmerId = (int) request.getData();
						FarmerProvider farmerProvider = new FarmerProvider();
						List<Crop> crops = farmerProvider.selectCrops(farmerId);
						
						if (crops == null)
							response = new Response(crops, false, "Unable to retrieve crops");
						else
							response = new Response(crops);
					}
					break;
					case "SELECTALLCROPFORCUSTOMER": {
						logger.info("Request: " + action);
						CropProvider cp = new CropProvider();
						List<Crop> crops = cp.selectAll();
						
						if(crops == null)
							response = new Response(crops, false, "Unable to retrieve crops");
						else
							response = new Response(crops);
					}
					break;
					case "DELETECROP": {
						logger.info("Request: " + action);
						int id = (int) request.getData();
						CropProvider cropProvider = new CropProvider();
						int success = cropProvider.delete(id);
						
						if (success != 1)
							response = new Response(id, false, "Unable to delete crop");
						else
							response = new Response(id);
					}
					break;
					
					case "ADDTOCART": {		//Don't use this, not working
						logger.info("Request: " + action);
						Customer customer = (Customer) request.getData();
						ShoppingItem shoppingitem =  (ShoppingItem) request.getData2();
						CartProvider cartprovider = new CartProvider();
						//int success = customerProvider.update(customer, customer.getId());
						int success = cartprovider.add(customer, shoppingitem);
						
						if (success != 1)
							response = new Response(customer, false, "Unable to add item to cart");
						else
							response = new Response(customer);
					}
					break;
			/*		case "UPDATEACCOUNTBALANCE": {		//Duplicate update account balance?
						logger.info("Request: " + action);
						Customer customer = (Customer) request.getData();
						CustomerProvider customerProvider = new CustomerProvider();
						int success = customerProvider.update(customer, customer.getId());
						
//						if (success != 1)
//							response = new Response(id, false, "Unable to delete crop");
//						else
//							response = new Response(id);
					}
					break;
					*/
				}
				
				System.out.println("SERVER LOG:Server sending response " + response);
				oos.writeObject(response);
			}
			close();
			clientManager.removeClient(username);
		} catch (IOException e) {
			e.printStackTrace();
			
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			
		}
		
	}
	
}