import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class ServerDriver {
	
	private static Logger logger = LogManager.getLogger(ServerDriver.class);
	
	public static void main(String[] args) {
		logger.info("Starting Server");
		new Server();
	}

}
