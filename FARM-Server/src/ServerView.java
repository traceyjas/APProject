import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ServerView extends JFrame implements ActionListener {
	
	private static final long serialVersionUID = 1L;
	private Logger logger = LogManager.getLogger(ServerView.class);
	
	public ServerView() {
		configureView();
	}
	
	private void configureView(){
		
		setTitle("Server Manager");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		JButton stopButton = new JButton("Stop");
		stopButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				logger.info("SERVER LOG: Server stopped running");
				System.exit(0);
			}
		});
		add(stopButton);
		
		setSize(250, 250);
		setVisible(true);
		
	}
	
	@Override
	public void actionPerformed(ActionEvent event) {
	
	}
}