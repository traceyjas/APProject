package database;

import models.Address;
import models.Crop;
import models.Farmer;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import viewmodels.FarmerCustomer;

import javax.swing.*;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class FarmerProvider extends SqlProvider<Farmer>{

	private static Logger logger = LogManager.getLogger(CropProvider.class);

	private static final String TABLE_NAME = "farmers";

	Farmer farmer = null;
	private int success = 0;

	public FarmerProvider() {
		super();
	}

	public Farmer login(String email, String password){
		try {
			statement = con.createStatement();
			query = "SELECT id, email, firstName, lastName, photo, street, town, parish FROM farmers WHERE email = '"+ email +"' AND password = '" + password +"'";
			logger.info(query);
			result = statement.executeQuery(query);

			while(result.next()) {
				farmer = new Farmer(result.getInt(1), result.getString(2), result.getString(3), result.getString(4), result.getBytes(5), new Address(result.getString(6), result.getString(7),result.getString(8)));
			}

		} catch (SQLException e) {
			logger.error(e.getMessage());
		} catch (Exception e) {
			logger.error(e.getMessage());
		}

		return farmer;

	}
	
	public List<FarmerCustomer> getCustomers(int farmerId){
		List<FarmerCustomer> farmerCustomers = new ArrayList<FarmerCustomer>();
		
		try{
			statement =  con.createStatement();
			query = "SELECT customers.firstName, customers.lastName, purchases.amount_spent from purchases JOIN customers ON purchases.customer_id = customers.id WHERE purchases.farmer_id =" + farmerId;
			logger.info(query);
			result = statement.executeQuery(query);
			
			while(result.next()){
				farmerCustomers.add(new FarmerCustomer(result.getString(1), result.getString(2), result.getFloat(3)));
			}
			
		}
		catch (SQLException ex) {
			logger.error(ex.getMessage());
			JOptionPane.showMessageDialog(null, "Could not retreive records", "SQL Exception", JOptionPane.ERROR_MESSAGE);
		}
		catch (Exception e){
			logger.error(e.getMessage());
			JOptionPane.showMessageDialog(null, e.getMessage(), "SQL Exception", JOptionPane.ERROR_MESSAGE);
		}
		
		return farmerCustomers;
	}
	
	public List<Crop> selectCrops(int farmerId) {
		
		List<Crop> crops = new ArrayList<Crop>();
		Crop crop;
		
		try{
			statement =  con.createStatement();
			query = "SELECT * FROM crops WHERE farmer_id=" + farmerId;
			logger.info(query);
			result = statement.executeQuery(query);
			
			while(result.next()){
				crop = new Crop(result.getInt(1), result.getString(2), result.getBytes(3), result.getFloat(4), result.getInt(6), result.getFloat(5), result.getInt(8));
				
				crops.add(crop);
			}
			
		}
		catch (SQLException ex) {
			logger.error(ex.getMessage());
			JOptionPane.showMessageDialog(null, "Could not retreive records", "SQL Exception", JOptionPane.ERROR_MESSAGE);
		}
		catch (Exception e){
			logger.error(e.getMessage());
			JOptionPane.showMessageDialog(null, e.getMessage(), "SQL Exception", JOptionPane.ERROR_MESSAGE);
		}
		
		return crops;
	}

	@Override
	public List<Farmer> selectAll() {
		List<Farmer> farmers = new ArrayList<Farmer>();
		
		try{
			statement =  con.createStatement();
			query = "SELECT * FROM farmers";
			logger.info(query);
			result = statement.executeQuery(query);
			
			while(result.next()){
				Farmer farmer = new Farmer(result.getInt(1), result.getString(2), result.getString(3), result.getString(4),null, new Address( "I left address out","I left address out","I left address out"/*result.getString(7), result.getString(8), result.getString(9)*/));
				/*Farmer farmer = new Farmer(0, null, null, null, null, null);
				farmer.setId(result.getInt(1));
				farmer.setEmail(result.getString(2));
				farmer.setFirstName(result.getString(3));
				//farmer.setLastName(result.getString(4));*/
				farmers.add(farmer);
			}
			
		}
		catch (SQLException ex) {
			logger.error(ex.getMessage());
			JOptionPane.showMessageDialog(null, "Could not retreive records", "SQL Exception", JOptionPane.ERROR_MESSAGE);
		}
		catch (Exception e){
			logger.error(e.getMessage());
			//System.out.println(e.getMessage());
			JOptionPane.showMessageDialog(null,e.getMessage(), "SQL Exception", JOptionPane.ERROR_MESSAGE);
		}
		
		return farmers;	}
	
	public List<Farmer> searchAll(String firstName,String Lastname) {
		List<Farmer> farmers = new ArrayList<Farmer>();
		Farmer farmer;
		
		try{
			statement =  con.createStatement();
			query = "SELECT id,email,firstName,lastName FROM farmers where firstName= '"+firstName+"' AND lastName='"+Lastname+"' ;" ;
			logger.info(query);
			result = statement.executeQuery(query);
			
			while(result.next()){
				farmer = new Farmer(result.getInt(1), result.getString(2), result.getString(3), result.getString(4),result.getBytes(6), new Address( result.getString(7), result.getString(8), result.getString(9)));
				farmers.add(farmer);
			}
			//if (farmers = null)
				
			
		}
		catch (SQLException ex) {
			logger.error(ex.getMessage());
			JOptionPane.showMessageDialog(null, "Could not retreive records", "SQL Exception", JOptionPane.ERROR_MESSAGE);
		}
		catch (Exception e){
			logger.error(e.getMessage());
			JOptionPane.showMessageDialog(null, e.getMessage(), "SQL Exception", JOptionPane.ERROR_MESSAGE);
		}
		
		return farmers;	}

	@Override
	public Farmer get(int id) {
		return null;
	}

	@Override
	public int update(Farmer item, int id) {
		return 0;
	}

	@Override
	public int delete(int id) {
		return 0;
	}
//Testing
	//@Override
	/*
	public int add(Farmer farmer) {
		try{
			statement = con.createStatement();
			String query = "INSERT INTO "+ TABLE_NAME +" (email, firstName, lastName, password, photo, street, town, parish) VALUES ('"
					+ farmer.getEmail() +"', '"
					+ farmer.getFirstName() + "', '"
					+ farmer.getLastName() +"', '"
					+ farmer.getPassword() +"', '"
					+ farmer.getPhoto() +"', '"
					+ farmer.getFarmAddress().getStreet() +"', '"
					+ farmer.getFarmAddress().getTown() +"', '"
					+ farmer.getFarmAddress().getParish() +"');";

			logger.info(query);
			PreparedStatement preparedStatement = con.prepareStatement(query);
			success = preparedStatement.executeUpdate();
			logger.info("Farmer Inserted");

		}catch(SQLException e){
			logger.error("Unable to add farmer",e);
		}

		return success;
	}
	*/
	@Override
	public int add(Farmer farmer) {
		//RequestAction.CREATE_FARMER
		try{
			statement = con.createStatement();
			String query = "INSERT INTO "+TABLE_NAME+" (email, firstName, lastName, password, photo, street, town, parish) VALUES(?,?,?,?,?,?,?,?)";
			logger.info(query);
			PreparedStatement pst1 = con.prepareStatement(query);
			pst1.setString(1,farmer.getEmail());
			pst1.setString(2, farmer.getFirstName());
			pst1.setString(3, farmer.getLastName());
			pst1.setString(4, farmer.getPassword());
			pst1.setBytes(5, farmer.getPhoto());
			pst1.setString(6, farmer.getFarmAddress().getStreet());
			pst1.setString(7, farmer.getFarmAddress().getTown());
			pst1.setString(8, farmer.getFarmAddress().getParish());
			success = pst1.executeUpdate();
			logger.info("Farmer Inserted");

		}catch(SQLException e){
			logger.error("Unable to add farmer",e);
		}

		return success;
	}



}
