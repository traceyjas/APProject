package database;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.sql.*;
import java.util.List;
import java.util.Scanner;

public abstract class SqlProvider<T> {

	protected static Connection con = null;
	protected static final String DRIVER = "org.sqlite.JDBC";
	protected static ResultSet result = null;
	protected static Statement statement = null;
	protected static String url = "jdbc:sqlite:APProject.sqlite";
	protected static String query = null;
	protected static Logger logger = LogManager.getLogger(SqlProvider.class);
	protected int success = 0;

	public SqlProvider() {

		try{
			logger.warn("Attempting to connect to database, errors may occur");
			Class.forName(DRIVER).newInstance();
			con = DriverManager.getConnection(url);

			try{
				initSQLDatabase();
			} catch (FileNotFoundException e) {
				logger.error("Unable to find SQL file", e);
			}

		}
		catch(SQLException e){
			logger.error("Could not connect to database",e.getMessage());
		}
		catch(ClassNotFoundException e){
			logger.error("Failed to load JDBC Driver",e.getMessage());
		}
		catch(NullPointerException e){
			logger.error("Could not find database",e.getMessage());
		}
		catch(IllegalAccessException e){
			logger.error("Unauthorized access to the database",e.getMessage());
		}
		catch(InstantiationException e){
			logger.error("Could not create instance of database",e.getMessage());
		}
		catch(Exception e){
			logger.error("Database Connection Failure",e.getMessage());
		}
		
		logger.info("Connected to database");
	}

	private void initSQLDatabase() throws FileNotFoundException, SQLException {
		
		statement = con.createStatement();
		
		String DATABASE_PATH = System.getProperty("user.dir") + File.separator + "FARM-Server" + File.separator + "database.sql" + File.separator;
		
		FileInputStream source = new FileInputStream(DATABASE_PATH);
		logger.info(source);
		Scanner scanner = new Scanner(source);
		scanner.useDelimiter(";");
		
		while(scanner.hasNext()){
			query = scanner.next();
			logger.info(query);
			statement.execute(query);
		}
		
		scanner.close();
		
		logger.debug("Database initialized");
		
	};

	abstract public List<T> selectAll();

	abstract public T get(int id);

	abstract public int update(T item, int id);

	abstract public int delete(int id);

	abstract public int add(T item);

}
