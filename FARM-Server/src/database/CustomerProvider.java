package database;

import models.Customer;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

public class CustomerProvider extends SqlProvider<Customer> {

	private static Logger logger = LogManager.getLogger(CustomerProvider.class);

	private static final String TABLE_NAME = "customers";

	Customer customer = null;

	private int success = 0;

	public CustomerProvider() {
		super();
	}

	public Customer login(String email, String password){
		try {
			statement = con.createStatement();
			query = "SELECT id, email, firstName, lastName, photo, accountBalance FROM customers WHERE email = '"+ email +"' AND password = '" + password +"'";
			logger.info(query);
			result = statement.executeQuery(query);

			while(result.next()) {
				customer = new Customer(result.getInt(1), result.getString(2), result.getString(3), result.getString(4), result.getBytes(5), result.getFloat(6));
			}

		} catch (SQLException e) {
			logger.error(e.getMessage());
		} catch (Exception e) {
			logger.error(e.getMessage());
		}

		return customer;

	}

	@Override
	public List<Customer> selectAll() {
		return null;
	}

	@Override
	public Customer get(int id) {
		return null;
	}

	@Override
	public int update(Customer item, int id) {
		try{
			String query = "UPDATE "+ TABLE_NAME +" SET "
					+ "firstName='" + item.getFirstName() + "', "
					+ "lastName='" + item.getFirstName() + "', "
					//+ "password='" + item.getImage() + "', "
					//+ "ema=" + item.getQuantity() + ", "
					//+ "photo=" + item.getPhoto() + ", "
					+ "accountBalance=" + item.getAccountBalance() + " "
					+ "WHERE id="+ item.getId() +";";
			//		+ "inStock='" + item.getInStock() + "', "
			//		+ "farmer_id=" + item.getFarmerId() +" "
					

			logger.info(query);
			success = statement.executeUpdate(query);
			logger.info("Record Updated");

		}catch(SQLException e){
			logger.error("Unable to update customer (Balance custprovider)",e);
		}

		return success;
		
	}

	@Override
	public int delete(int id) {
		return 0;
	}

	/*
	@Override
	public int add(Customer customer) {
		try{
			String query = "INSERT INTO "+ TABLE_NAME +" (email, firstName, lastName, password, photo, accountBalance) VALUES ('"
					+ customer.getEmail() +"', '"
					+ customer.getFirstName() + "', '"
					+ customer.getLastName() +"', '"
					+ customer.getPassword() +"', '"
					+ customer.getPhoto() +"', '"
					+ customer.getAccountBalance()  +"');";

			logger.info(query);
			PreparedStatement preparedStatement = con.prepareStatement(query);
			success = preparedStatement.executeUpdate();
			logger.info("Customer Inserted");

		}catch(SQLException e){
			logger.error("Unable to add customer",e);
		}

		return success;
	}
	*/
	@Override
	public int add(Customer customer)
	{
		try{
			statement = con.createStatement();
			String query = "INSERT INTO "+ TABLE_NAME +" (email, firstName, lastName, password, photo, accountBalance) VALUES(?,?,?,?,?,?)";
			logger.info(query);
			PreparedStatement pst1 = con.prepareStatement(query);
			pst1.setString(1, customer.getEmail());
			pst1.setString(2,customer.getFirstName());
			pst1.setString(3,customer.getLastName());
			pst1.setString(4,customer.getPassword());
			pst1.setBytes(5,customer.getPhoto());
			pst1.setFloat(6,customer.getAccountBalance());
			success = pst1.executeUpdate();
			logger.info("Customer inserted");

		}catch(SQLException e){
			logger.error("Unable to add customer",e);
		}

		return success;
	}

}
