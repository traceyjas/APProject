package database;

import models.Customer;
import models.ShoppingItem;
import models.ShoppingCart;
import java.sql.SQLException;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.sql.PreparedStatement;


public class CartProvider extends SqlProvider<ShoppingItem> {
	
	private static Logger logger = LogManager.getLogger(CartProvider.class);

	private static final String TABLE_NAME = "cartItems";

	ShoppingCart cart = null;

	private int success = 0;

	@Override
	public List<ShoppingItem> selectAll() {
		return null;
	}

	@Override
	public ShoppingItem get(int id) {
		return null;
	}

	@Override
	public int update(ShoppingItem item, int id) {
		return 0;
	}


	public int updateCart(Customer item) {		//Not completed, fix or do not use
		try{
			String query = "UPDATE "+ TABLE_NAME +" SET "
		//			+ "customer_id='" + item.getFirstName() + "', "
					+ "crop_id='" + item.getFirstName() + "', "
					//+ "password='" + item.getImage() + "', "
					//+ "ema=" + item.getQuantity() + ", "
					//+ "photo=" + item.getPhoto() + ", "
					+ "quantity=" + item.getAccountBalance() + " "
					+ "WHERE customer_id="+ item.getId() +";";
		
					

			logger.info(query);
			success = statement.executeUpdate(query);
			logger.info("Record Updated");

		}catch(SQLException e){
			logger.error("Unable to update customer (Balance custprovider)",e);
		}

		return success;
		
	}

	@Override
	public int delete(int id) {
		return 0;
	}

	//@Override
	public int add(Customer customer, ShoppingItem item) {
		try{
			statement = con.createStatement();
			String query = "INSERT INTO "+ TABLE_NAME +" (customer_id, crop_id, quantity) VALUES(?,?,?)";
			logger.info(query);
			PreparedStatement pst1 = con.prepareStatement(query);
			pst1.setInt(1, customer.getId());
			pst1.setInt(2,item.getCrop().getId());
			pst1.setInt(3,item.getQuantity());
		//	pst1.setString(4,customer.getPassword());
		//	pst1.setBytes(5,customer.getPhoto());
		//	pst1.setFloat(6,customer.getAccountBalance());
			success = pst1.executeUpdate();
			logger.info("Cart item inserted");

		}catch(SQLException e){
			logger.error("Unable to add customer",e);
		}

		return success;
	
	}

	@Override
	public int add(ShoppingItem item) {
		// TODO Auto-generated method stub
		return 0;
	}
	
	
}
