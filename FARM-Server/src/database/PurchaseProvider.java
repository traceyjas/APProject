package database;

import models.Purchase;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;

public class PurchaseProvider extends SqlProvider<Purchase>{

	private static Logger logger = LogManager.getLogger(PurchaseProvider.class);
	private static final String TABLE_NAME = "farmers";
	Purchase purchase = null;
	private int success = 0;

	public PurchaseProvider() {
		super();
	}

	@Override
	public List<Purchase> selectAll() {

		return null;
	}

	public List<Purchase> selectAll(int farmerId) {

		return null;
	}

	@Override
	public Purchase get(int id) {
		return null;
	}

	@Override
	public int update(Purchase purchase, int id) {
		return 0;
	}

	@Override
	public int delete(int id) {
		return 0;
	}

	@Override
	public int add(Purchase purchase) {
		return 0;
	}
}
