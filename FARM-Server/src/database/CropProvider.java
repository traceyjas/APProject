package database;


import models.Crop;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class CropProvider extends SqlProvider<Crop> {

	private static Logger logger = LogManager.getLogger(CropProvider.class);
	private static final String TABLE_NAME = "crops";
	private Crop crop = null;

	public CropProvider() {
		super();
	}

	@Override
	public List<Crop> selectAll() {

		List<Crop> crops = new ArrayList<Crop>();

		try{
			statement =  con.createStatement();
			query = "SELECT * FROM "+ TABLE_NAME;
			logger.info(query);
			result = statement.executeQuery(query);

			while(result.next()){
				crop = new Crop(result.getInt(1), result.getString(2), result.getBytes(3), result.getFloat(4), result.getInt(6), result.getFloat(5), result.getInt(8));

				crops.add(crop);
			}

		}
		catch (SQLException ex) {
			logger.error(ex.getMessage());
			JOptionPane.showMessageDialog(null, "Could not retreive records", "SQL Exception", JOptionPane.ERROR_MESSAGE);
		}
		catch (Exception e){
			logger.error(e.getMessage());
			JOptionPane.showMessageDialog(null, e.getMessage(), "SQL Exception", JOptionPane.ERROR_MESSAGE);
		}

		return crops;
	}
	
	/*public List<String> getCropNames(){
		List<String> names = new ArrayList<String>();
		String getName = null;
		
		try{
			statement = con.createStatement();
			query = "SELECT name FROM crops";
			logger.info(query);
			result = statement.executeQuery(query);
			
			while(result.next()){
				getName = result.getString(2);
				names.add(getName);
			}
		}catch(SQLException e){
			logger.info(e.getMessage());
			JOptionPane.showMessageDialog(null, "Could not retrieve names of crops","SQL Exception", JOptionPane.ERROR_MESSAGE);
		}
		catch(Exception e){
			logger.error(e.getMessage());
			JOptionPane.showMessageDialog(null, e.getMessage(),"SQL Exception", JOptionPane.ERROR_MESSAGE);
		}
		return names;
	}*/

	@Override
	public Crop get(int id) {
		try
		{
			statement = con.createStatement();
			query = "SELECT * FROM "+ TABLE_NAME +" WHERE id="+ id;
			logger.info(query);
			result = statement.executeQuery(query);

			while(result.next()){
				crop = new Crop(result.getInt(1), result.getString(2), result.getBytes(3), result.getFloat(4), result.getInt(6), result.getFloat(5), result.getInt(8));
			}
		}
		catch (SQLException ex) {
			logger.error(ex.getMessage());
			JOptionPane.showMessageDialog(null, "Could not retreive record", "SQL Exception", JOptionPane.ERROR_MESSAGE);
		}
		catch (Exception e){
			logger.error(e.getMessage());
			JOptionPane.showMessageDialog(null, e.getMessage(), "SQL Exception", JOptionPane.ERROR_MESSAGE);
		}
		return crop;
	}

	@Override
	public int update(Crop item, int id) {
		try{
			String query = "UPDATE "+ TABLE_NAME +" SET "
					+ "name='" + item.getName() + "', "
					+ "image='" + item.getImage() + "', "
					+ "weight=" + item.getWeight() + ", "
					+ "cost=" + item.getCost() + ", "
					+ "quantity=" + item.getQuantity() + ", "
					+ "inStock='" + item.getInStock() + "', "
					+ "farmer_id=" + item.getFarmerId() +" "
					+ "WHERE id="+ item.getId() +";";

			logger.info(query);
			success = statement.executeUpdate(query);
			logger.info("Record Updated");

		}catch(SQLException e){
			JOptionPane.showMessageDialog(null, "Update unsuccessful","SQL Exception", JOptionPane.ERROR_MESSAGE);
			logger.error("Unable to update crop",e);
		}catch(Exception e){
			logger.error(e.getMessage());
		}

		return success;
	}

	@Override
	public int delete(int id) {
		try
		{
			statement = con.createStatement();
			String query = "DELETE FROM " + TABLE_NAME + " WHERE id = "+id;
			logger.info(query);
			success = statement.executeUpdate(query);
		}
		catch (SQLException ex) {
			logger.error(ex.getMessage());
			JOptionPane.showMessageDialog(null, "Could not delete record", "SQL Exception", JOptionPane.ERROR_MESSAGE);
		}
		catch (Exception e){
			logger.error(e.getMessage());
			JOptionPane.showMessageDialog(null, e.getMessage(), "SQL Exception", JOptionPane.ERROR_MESSAGE);
		}
		return success;
	}

	@Override
	public int add(Crop crop) {
		try{
			statement = con.createStatement();
			String query = "INSERT INTO "+TABLE_NAME+" (name, image, weight, cost, quantity, inStock, farmer_id) VALUES(?,?,?,?,?,?,?)";
			logger.info(query);
			PreparedStatement pst1 = con.prepareStatement(query);
			pst1.setString(1,crop.getName());
			pst1.setBytes(2, crop.getImage());
			pst1.setFloat(3, crop.getWeight());
			pst1.setFloat(4, crop.getCost());
			pst1.setInt(5,crop.getQuantity());
			pst1.setBoolean(6, crop.getInStock());
			pst1.setInt(7, crop.getFarmerId());
			success = pst1.executeUpdate();
			logger.info("Crop Inserted");
		}catch(SQLException e){
			logger.error("Unable to add crop",e);
		}

		return success;
	}


}
