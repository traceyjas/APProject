import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class Server {
	
	private ServerSocket serverSocket;
	private Socket socket;
	private ClientManager clientManager;
	
	private Logger logger = LogManager.getLogger(Server.class);
	
	public Server() {
		logger.info("SERVER LOG: Server running");
		new ServerView();
		clientManager = ClientManager.getInstance();
		createConnection();
	}
	
	private void createConnection() {
		try {
			serverSocket = new ServerSocket(9000, 3);
			waitForRequests();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private void waitForRequests() {
		logger.info("Waiting for requests");
		String name;
		try {
			while (true) {
				socket = serverSocket.accept();
				name = (Math.random() * 9000) + "";
				clientManager.addClient(name, new ClientConnection(name,socket));
				logger.info("SERVER LOG: " + name + " connected to server");
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
