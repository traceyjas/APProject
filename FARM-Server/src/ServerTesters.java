import database.FarmerProvider;
import models.Crop;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class ServerTesters {
	
	private static Logger logger = LogManager.getLogger(ServerTesters.class);
	
	public static void main(String[] args) {
		
		FarmerProvider farmerProvider = new FarmerProvider();
		
		for (Crop crop : farmerProvider.selectCrops(1)) {
			System.out.println(crop);
		}
	}

}
