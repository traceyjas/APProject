import java.util.HashMap;
import java.util.Map;

public class ClientManager {
	
	private Map<String,ClientConnection> clients = null;
	private static ClientManager instance= null;
	
	public static ClientManager getInstance(){
		if(instance == null){
			instance = new ClientManager();
		}
		return instance;
	}
	
	private ClientManager(){
		clients = new HashMap<String,ClientConnection>();
	}
	
	public void addClient(String username,ClientConnection con){
		//should check if name is already in use
		clients.put(username, con);
	}
	
	public void removeClient(String username){
		clients.remove(username);
	}
	
	public ClientConnection getClient(String username){
		return clients.get(username);
	}
	
	public boolean updateClientUsername(String oldUsername, String newUsername){
		if(clients.containsKey(oldUsername)){
			ClientConnection temp = clients.get(oldUsername);
			clients.remove(oldUsername);
			//should check if username already in use
			clients.put(newUsername, temp);
			System.out.println("Username updated from "+oldUsername+" to "+newUsername);
		}
		return true;
	}
	
	public boolean userExists(String receiver) {
		return clients.containsKey(receiver);
	}
	
}
