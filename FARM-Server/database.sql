CREATE TABLE IF NOT EXISTS farmers
(
    id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
    email VARCHAR(255) NOT NULL,
    firstName VARCHAR(255),
    lastName VARCHAR(255),
    password VARCHAR(255) NOT NULL,
    photo INTEGER,
    street VARCHAR(255),
    town VARCHAR(255),
    parish VARCHAR(255)
);

CREATE TABLE IF NOT EXISTS crops
(
    id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
    name VARCHAR(255) NOT NULL,
    image INTEGER,
    weight DOUBLE(10,2),
    cost DOUBLE(10,2),
    quantity INTEGER DEFAULT '0' NOT NULL,
    inStock TINYINTEGER(1) DEFAULT '0' NOT NULL,
    farmer_id INTEGER NOT NULL,
    CONSTRAINT crops_farmers_id_fk FOREIGN KEY (farmer_id) REFERENCES  farmers(id) ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE TABLE IF NOT EXISTS customers
(
    id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
    email VARCHAR(255) NOT NULL,
    firstName VARCHAR(255),
    lastName VARCHAR(255),
    password VARCHAR(255) NOT NULL,
    photo INTEGER,
    accountBalance DOUBLE(10,2) NOT NULL
);

CREATE TABLE IF NOT EXISTS purchases
(
    farmer_id INTEGER,
    customer_id INTEGER,
    amount_spent DOUBLE(10,2) NOT NULL,
    CONSTRAINT purchases_farmers_id_fk FOREIGN KEY (farmer_id) REFERENCES farmers (id) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT purchases_customers_id_fk FOREIGN KEY (customer_id) REFERENCES customers (id) ON DELETE CASCADE ON UPDATE CASCADE,
    PRIMARY KEY (farmer_id, customer_id, amount_spent)
);

CREATE TABLE IF NOT EXISTS cartItems
(
    customer_id INTEGER NOT NULL,
    crop_id INTEGER NOT NULL,
    quantity INTEGER NOT NULL,
    CONSTRAINT cartItems_customers_id_fk FOREIGN KEY (customer_id) REFERENCES customers (id) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT cartItems_crops_id_fk FOREIGN KEY (crop_id) REFERENCES crops (id) ON DELETE CASCADE ON UPDATE CASCADE,
    PRIMARY KEY (customer_id, crop_id)
);